﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
    class Comidas
    {
        public int Id { get; set; }
        public int IdCategoriasComidas { get; set; }
        public string Nombre { get; set; }
        public float Cal { get; set; }
        public float Grasas { get; set; }
        public float Azucares { get; set; }
    }
}
