﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
    class ListaComidas
    {
        //Atributos de la clase ListaActividades
        public int IdComidas { get; set; }
        public int Racion { get; set; }
        public DateTime Fecha { get; set; }
        public int IdTamagochi { get; set; }
        public string NombreComidas { get; set; }
    }
}
