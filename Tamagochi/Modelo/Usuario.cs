﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Tamagochi.Modelo
{

    // clase publica Usuario con sus atributos y otras cosas

    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public int Peso { get; set; }
        public string Contrasenya { get; set; }
        public string ConfirmPassword { get; set; }
        public string email { get; set; }

        public static Usuario Usuario_Actual = null;

        // atributo (modelo) Tamagochi con sus atributos
        public Tamagochi Tamagochis { get; set; }
        
    }
}
