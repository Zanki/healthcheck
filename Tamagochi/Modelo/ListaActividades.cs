﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
    public class ListaActividades
    {
        //Atributos de la clase ListaActividades
        public int IdActividades { get; set; }
        public DateTime Fecha { get; set; }
        public int IdTamagochi { get; set; }
        public int Duracion { get; set; }
        
        public string NombreActividad { get; set; }

        //Atributos de la clase LlistaActividades 
        public List<Actividades> actividades = new List<Actividades>();

        //Método que permite consultar la lista
        public Actividades ConsultarLista(int a)
        {
            Actividades act = new Actividades();
            act = this.actividades[a];
            return act;
        }

        //Método que devuelve el número de actividades de la lista
        public int GetNum()
        {
            return this.actividades.Count;
        }
    }
}
