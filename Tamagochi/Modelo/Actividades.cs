﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
    public class Actividades
    {
        //Atributos de la clase
        public string intensidad { get; set; }
        public float duracion { get; set; }
        public string nombre { get; set; }
        public int saciedad { get; set; }
        public int mental { get; set; }
        public int conocimiento { get; set; }
        public int categoria { get; set; }
        public int cansancio { get; set; }
        public int higiene { get; set; }
        public int fisico { get; set; }
        public double calorias{ get; set; }
        public double grasas { get; set; }
        public double azucar { get; set; }
        public int id { get; set; }

        public int[] toArray()
        {
            return  new int[]
            {
                this.cansancio,
                this.saciedad,
                this.mental,
                this.conocimiento,
                this.higiene,
                this.fisico
            };
        }
        //Constructor que inicializa la clase a partir de todas sus variables
        public Actividades(int id, string intensidad, float duracion, string nombre, int saciedad, int mental, int conocimiento, int categoria, int cansancio, int higiene, int fisico, double calorias, double grasas, double azucar)
        {
            this.id = id;
            this.intensidad = intensidad;
            this.duracion = duracion;
            this.nombre = nombre;
            this.saciedad = saciedad;
            this.mental = mental;
            this.conocimiento = conocimiento;
            this.categoria = categoria;
            this.cansancio = cansancio;
            this.higiene = higiene;
            this.fisico = fisico;
            this.calorias = calorias;
            this.grasas = grasas;
            this.azucar = azucar;
        }

        public Actividades()
        {
        }

        //Tiempo que duración de la actividad
        public int Duracion(int tiempo)
        {
            if(tiempo > 0)
            {

            }
            return tiempo;
        }
    }
}
