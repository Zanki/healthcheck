﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
   public class Tamagochi
    {
        public int Id { get; set; }
        public int Hambre { get; set; }
        public int Cansancio { get; set; }
        public int Higiene { get; set; }
        public int FormaFisica { get; set; }
        public int SaludMental { get; set; }
        public int Conocimientos { get; set; }
        public int MediaGeneral { get; set; }
        public DateTime data { get; set; }
        public DateTime DataRegistro { get; set; }



        public double TiempoHastaActualizacion { get; set; }

        public static Tamagochi Tamagochi_Actual = null;

    }
}
