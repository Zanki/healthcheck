﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamagochi.Modelo
{
    public class Grafico
    {
        public int IdTamagochi { get; set; }
        public int IdActividad { get; set; } 
        public int Hambre { get; set; }
        public int Cansancio { get; set; }
        public int Higiene { get; set; }
        public int FormaFisica { get; set; }
        public int SaludMental { get; set; }
        public int Conocimientos { get; set; }
        public int MediaGeneral { get; set; }
        public DateTime Fecha { get; set; }
    }
}
