﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tamagochi.Modelo
{
    public static class Valida
    {

        public static bool Validatxt(Panel p1)
        {
            foreach (Control c in p1.Controls)
            {
                if (c is TextBox)
                {
                    TextBox aux = (TextBox)c;
                    if (String.IsNullOrEmpty(aux.Text.Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

    }
}
