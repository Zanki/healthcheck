﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;


namespace Tamagochi.Vista
{
    public partial class Contraseña : Form
    {
        
        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";
        private string contraseña;

        public string GetContraseña()
        {
            return contraseña;
        }

        private void SetContraseña(string value)
        {
            contraseña = value;
        }

        public Contraseña()
        {
            InitializeComponent();
        }

        //Método que genera una nueva contraseña de forma aleatoria
        public void GenerarNuevaContraseña(string email)
        {
            Random rd = new Random(DateTime.Now.Millisecond);
            int nuevaCont = rd.Next(100000, 999999);

            string passwordVisible = nuevaCont.ToString();
            string passwordCodificada = Codifica.ConverteixPassword(passwordVisible);

            MySqlConnection cnx = new MySqlConnection(connStr);

            string tabla = "usuario";
            string sql = string.Format("UPDATE {0} SET Contrasenya= '{1}' WHERE email='{2}'", tabla, passwordCodificada, email);
            MySqlCommand cmd = new MySqlCommand(sql, cnx);
            //cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@contrasenya", passwordCodificada);

            try
            {
                cnx.Open();
                int filas = cmd.ExecuteNonQuery(); //Nos dice el número de filas afectadas en nuestro caso tiene que ser 1
                if (filas != 0)
                {
                    EnviarEmailContraseña(nuevaCont, email);
                }
            }
            catch
            {

            }
        }

       
        /*De momento devuelve la contraseña en un MessageBox si hay tiempo hacer que llegue por correo*/
        //Método que envía un correo electrónico con la nueva contraseña
        public void EnviarEmailContraseña(int contNueva, string correo)
        {
            string destinatario = correo;
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("mariagayafiol@gmail.com", "quejaleo");

            string remitente = "mariagf_1997@hotmail.com";
            string asunto = "Nueva contraseña HealthCheck";
            string cuerpo = "Estimado cliente al final de este email podrá encontrar su nueva contraseña " + ". \n Le recordamos que una vez iniciada la sesión podrá canbiarla. \n\n Atentament, \n\n Equipo técnico HealthCheck \n\n" + Convert.ToString(contNueva);
            MailMessage mm = new MailMessage("mariagaya@gmail.com", correo, asunto, cuerpo);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            try
            {
                MessageBox.Show("Puede tardar unos segundos.");
                client.Send(mm);
                mm.Dispose();
                MessageBox.Show("Correo electrónico enviado, por favor revise su email.");
            }
            catch(SmtpException ex)
            {
                
                MessageBox.Show("Error al enviar el correo electrónico." + ex.Message);
            }
        }

        //Botón recuperar. Al introducir el correo electrónico compara si este se encuentra en
        //la base de datos, si es así procede al método nueva constraseña
        private void button1_Click(object sender, EventArgs e)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            string tabla = "usuario";
            string sql = string.Format("SELECT * FROM {0} WHERE email='{1}'", tabla, textBoxEmail.Text);

            MySqlCommand cmd = new MySqlCommand(sql, _conn);

   
             //cmd.CommandType = CommandType.StoredProcedure;
             cmd.Parameters.AddWithValue("@email", textBoxEmail.Text);

            try
            {
                _conn.Open();
                MySqlDataReader leer = cmd.ExecuteReader();
                if (leer.Read())
                {
                    GenerarNuevaContraseña(textBoxEmail.Text);
                }
                else
                {
                    MessageBox.Show("Correo electrónico no encontrado.");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Contraseña_Load(object sender, EventArgs e)
        {
            AcceptButton = button1;
        }

        private void Contraseña_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }

}

