﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;
using Tamagochi.Controlador;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace Tamagochi.Vista
{
    public partial class UsuarioNuevo : Form
    {

        //Declaramos Controlador
        private UsuarioController user = new UsuarioController();
        private TamagochiController ta = new TamagochiController();


        public UsuarioNuevo()
        {
            InitializeComponent();
        }

        //Método que comprueba si el email es correcto
        static bool validarEmail(string email)
        {
            try
            {
                new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        //Boton de guardar datos que introduce usuario
        private void button1_Click(object sender, EventArgs e)
        {
            bool llenos = Valida.Validatxt(panel1);
            
            if (llenos == true)
            {
                Usuario a = new Usuario();
                a.Nombre = textBox1.Text;
                a.Edad = Convert.ToInt32(textBox2.Text);
                a.Peso = Convert.ToInt32(textBox3.Text);
                a.Contrasenya = textBoxContrasenya.Text;
                a.email = textBoxEmail.Text;
                // comprobamos que nombre sea unico y lo guardamos en bd si está bien
                if (user.NombreLibre(a.Nombre))
                {
                    //Comprobamos que la edad y el peso esten en los rangos adecuados 
                    // Y guardamos el usuario en la Base de Datos
                    if (a.Edad > 0 && a.Edad < 150)
                    {
                        if (a.Peso > 0 && a.Peso < 300)
                        {
                            if(validarEmail(a.email))
                            {
                                if (textBoxContrasenya.Text == textBoxContrasenya2.Text)
                                {
                                    // guardamos el usuario como nuevo y cerramos
                                    user.Save(a);

                                    //Coge el usuario para saber su id
                                    a = user.GetUserByNombre(a.Nombre);
                                    // I creamos el tamagochi para ese usuario
                                    ta.CreateTamagochi(a.Id);

                                    this.Close();
                                    UsuarioGeneral w = new UsuarioGeneral();
                                    // w.Show();
                                }
                                else
                                {
                                    MessageBox.Show("Las contraseñas introducidas son diferentes.");
                                }
                            }
                            else
                            {
                                MessageBox.Show("El correo electrónico introducido no es válido.");
                            }

                        }
                        else {
                                MessageBox.Show("Error: Peso incorrecto.");
                            }
                    }
                    else
                    {
                        MessageBox.Show("Error: Edad incorrecta.");
                    }
                }
                else
                {
                    MessageBox.Show("Este nombre ya corresponde a otro usuasio. ");
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son obligatorios.");
            }
                  
        }
        

        //Limita textbox a cantidad de digitos
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.MaxLength = 30;
            
        }

        private void solo_numeros(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void UsuarioNuevo_Load(object sender, EventArgs e)
        {

        }

        private void UsuarioNuevo_Load_1(object sender, EventArgs e)
        {

        }
    }
}



// if (a.email.IndexOf('@') != -1 && a.email.IndexOf('.') != -1)