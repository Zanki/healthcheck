﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using Tamagochi.Modelo;
using Tamagochi.Controlador;
using System.Media;
using System.Threading;

namespace Tamagochi.Vista
{
    public partial class NuevoRegistro : Form
    {

        int actvalue = 0, catvalue = 0, catcomvalue = 0, comvalue = 0;
        ActividadesController ac = new ActividadesController();
        ComidasController comc = new ComidasController();
        ListaActividadesController listac = new ListaActividadesController();
        TamagochiCentral tamacentral;

        public NuevoRegistro()
        {
            InitializeComponent();
        }

        //Método que permite registrar una actividad
        public NuevoRegistro(string numact, string nomact)
        {
            InitializeComponent();

            Actividades act = new Actividades();
            act = ac.GetActividadByName(nomact);
            if (!BoxActividades.Enabled) BoxActividades.Enabled = true;
            comboBoxCategoria.SelectedValue = act.categoria;
            BoxActividades.SelectedValue = numact;
        }

        public NuevoRegistro(TamagochiCentral tc)
        {
            InitializeComponent();
            this.tamacentral = tc;            
        }
        

        /*Per canviar el color

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            int index = e.Index >= 0 ? e.Index : 0;
            var brush = Brushes.Black;
            e.DrawBackground();
            e.Graphics.DrawString(comboBox1.Items[index].ToString(), e.Font, brush, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }
        */

        
        private void NuevoRegistro_Load(object sender, EventArgs e)
        {
            ActividadesController ca = new ActividadesController();

            ca.rellenarCombobox(comboBoxCategoria);
            comc.RellenarCatComida(comboBoxCatComida);

            comboBoxCatComida.Items.RemoveAt(0);

            BoxActividades.Enabled = false;

            if (BoxActividades.SelectedValue!=null) BoxActividades.Enabled = true;

            BoxDuracion.Enabled = false;
            comboBoxComida.Enabled = false;
            comboBoxRaciones.Enabled = false;
    
            for (int i = 1; i < 8; i++)
            {
                comboBoxRaciones.Items.Add(i);
            }
        }

        #region botones guardar

        private void Guardar_Click(object sender, EventArgs e)
        {           
            TamagochiController tam = new TamagochiController();
            ListaActividadesController listact = new ListaActividadesController();
            DateTime dat = DateTime.Now;
            Actividades activ = new Actividades();
            List<ListaActividades> ll = new List<ListaActividades>();
           

            int Duracion, id = 0;
            string Categoria, Actividad;
            
            /* 
            int[] val = new int[9] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            Cat = (int)comboBoxCategoria.SelectedValue;
            CatCom = (int)comboBoxCatComida.SelectedValue;
            Comida = comboBoxComida.SelectedText;
            */

            Actividad = BoxActividades.GetItemText(BoxActividades.SelectedItem); //BoxActividades.Text;
            Categoria = comboBoxCategoria.GetItemText(comboBoxCategoria.SelectedItem);

            //Todo
            if(Categoria != "")
            {
                if (Actividad != "")
                {
                    activ = ac.GetActividadByName(Actividad);

                    if (BoxDuracion.Enabled)
                    {
                        if (BoxDuracion.GetItemText(BoxDuracion.SelectedItem) != "")
                        {
                            Duracion = (int)BoxDuracion.SelectedIndex;
                            Duracion++;

                            if (Duracion == 0)
                            {
                                Duracion = 1;

                                activ.saciedad *= Duracion;
                                activ.cansancio *= Duracion;
                                activ.higiene *= Duracion;
                                activ.fisico *= Duracion;
                                activ.mental *= Duracion;
                                activ.conocimiento *= Duracion;
                            }

                            tam.ActualizarEstadosTamagochiUsuarioActual(activ);
                            MessageBox.Show("Actividad registrada correctamente.");

                            listac.Añadir(activ.id, dat, Usuario.Usuario_Actual.Tamagochis.Id, Duracion);

                            ll = listact.GetAllParaHistorial();
                            tamacentral.dGV.DataSource = ll;

                            TamagochiController n = new TamagochiController();
                            n.Avisos();

                            // CAMBIAR IMAGEN
                            id = ac.GetIdByName(Categoria);
                            this.ac.CambioImagenActividad(this.tamacentral, id);

                        }
                        else
                        {
                            MessageBox.Show("Introduzca una duración.");
                        }
                    }
                    else
                    {
                        //Actualiza valores del Tamagochi
                        tam.ActualizarEstadosTamagochiUsuarioActual(activ);

                        MessageBox.Show("Actividad registrada correctamente.");
                        int dur = 0;
                        //Guardar l'activitat a la llista HasActivities. (amb la data i hora).
                        listac.Añadir(activ.id, dat, Usuario.Usuario_Actual.Tamagochis.Id, dur);

                        //Actuaiza el historial en pantalla
                        ll = listact.GetAllParaHistorial();
                        tamacentral.dGV.DataSource = ll;

                        TamagochiController n = new TamagochiController();
                        n.Avisos();
                     
                        // CAMBIAR IMAGEN
                        id = ac.GetIdByName(Categoria);
                        this.ac.CambioImagenActividad(this.tamacentral, id);

                    }
                }
                else
                {
                    MessageBox.Show("Introduzca una actividad.");
                }
            }
            else
            {
                MessageBox.Show("Introduzca una categoria.");
            }           
        }

        private void GuardarComida_Click(object sender, EventArgs e)
        {
           
            TamagochiController tam = new TamagochiController();
            ListaComidasController listcom = new ListaComidasController();
            DateTime dat = DateTime.Now;
            Comidas comida = new Comidas();
            List<ListaComidas> lcomida = new List<ListaComidas>();

            int Raciones;
            string Comida, catComida, Racion;

            /*
            float[] val = new float[9] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //Cat = (int)comboBoxCategoria.SelectedValue;
            //CatCom = (int)comboBoxCatComida.SelectedValue;
            //Comida = comboBoxComida.SelectedText;
            */

            catComida = comboBoxCatComida.GetItemText(comboBoxCatComida.SelectedItem);
            Comida = comboBoxComida.GetItemText(comboBoxComida.SelectedItem);
            Racion = comboBoxRaciones.GetItemText(comboBoxRaciones.SelectedItem);

            Raciones = (int)comboBoxRaciones.SelectedIndex;
            Raciones++;

            if (catComida != "")
            {
                if(Comida != "")
                {
                    if(Racion != "")
                    {
                        if (Raciones == 0) Raciones = 1;
                        comida = comc.GetValuesFromComida(Comida);

                        comida.Grasas *= Raciones;
                        comida.Cal *= Raciones;
                        comida.Azucares *= Raciones;

                        MessageBox.Show("" + comida.Cal + " " + comida.Grasas + " " + comida.Azucares + " ");

                        //Guardar comida
                        
                        listcom.AñadirComida(comida.Id, dat,Raciones, Usuario.Usuario_Actual.Tamagochis.Id);

                        //Actuaiza el historial en pantalla
                        
                        lcomida = listcom.GetAllHistorialComidas();
                        tamacentral.dGV1.DataSource = lcomida;

                        TamagochiController tc = new TamagochiController();
                        tc.AvisosComida();
                    }
                    else
                    {
                        MessageBox.Show("Introduzca la cantidad de raciones.");
                    }
                }
                else
                {
                    MessageBox.Show("Introduzca una comida.");
                }
            }
            else
            {
                MessageBox.Show("Introduzca una categoria.");
            }

        }

        #endregion

        #region IndexChange Actividades

        private void comboBoxCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            catvalue = comboBoxCategoria.SelectedIndex;
            catvalue++;

            if(catvalue > 0)
            {
                ac.VaciarComboBox(BoxActividades);
                ac.VaciarComboBox(BoxDuracion);

                BoxActividades.Enabled = true;
                BoxDuracion.Enabled = false;

                ac.RellenarByCategoriaId(BoxActividades, catvalue);
            }
        }

        private void BoxActividades_SelectedIndexChanged(object sender, EventArgs e)
        {
            actvalue = BoxActividades.SelectedIndex;
            actvalue++;

            if ((catvalue == 2 || catvalue == 5 || catvalue == 7) && actvalue > 0)
            {
                ac.VaciarComboBox(BoxDuracion);

                BoxDuracion.Enabled = true;

                if (catvalue == 2) // Reposo 
                {
                    for (int i = 1; i < 8; i++)
                    {
                        BoxDuracion.Items.Add(i + " horas");
                    }

                }
                else if(catvalue == 5) // Trabajo
                {
                    for (int i = 1; i < 8; i++)
                    {
                        BoxDuracion.Items.Add(i + " horas");
                    }
                }
                else if(catvalue == 7) //Actividad Física
                {
                    for (int i = 1; i < 15; i++)
                    {
                        BoxDuracion.Items.Add(10*i + " minutos");
                    }
                }

            }
        }

        #endregion

        #region IndexChange Comidas

        private void comboBoxCatComida_SelectedIndexChanged(object sender, EventArgs e)
        {
            catcomvalue = comboBoxCatComida.SelectedIndex;
            catcomvalue++;

            ac.VaciarComboBox(comboBoxComida);

            comc.RellenarByCategoriaComidas(comboBoxComida, catcomvalue);

            comboBoxRaciones.Enabled = false;
            comboBoxComida.Enabled = true;            
        }

        private void comboBoxComida_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxRaciones.Enabled = true;
        }

        #endregion



        private void buttonvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
  
    }       
}
