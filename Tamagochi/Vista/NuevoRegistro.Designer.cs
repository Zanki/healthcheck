﻿namespace Tamagochi.Vista
{
    partial class NuevoRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxCategoria = new System.Windows.Forms.ComboBox();
            this.BoxDuracion = new System.Windows.Forms.ComboBox();
            this.LabActividad = new System.Windows.Forms.Label();
            this.LabDuracion = new System.Windows.Forms.Label();
            this.Guardar = new System.Windows.Forms.Button();
            this.comboBoxCatComida = new System.Windows.Forms.ComboBox();
            this.comboBoxComida = new System.Windows.Forms.ComboBox();
            this.LabComida = new System.Windows.Forms.Label();
            this.BoxActividades = new System.Windows.Forms.ComboBox();
            this.labCategoria = new System.Windows.Forms.Label();
            this.labCatComida = new System.Windows.Forms.Label();
            this.GuardarComida = new System.Windows.Forms.Button();
            this.comboBoxRaciones = new System.Windows.Forms.ComboBox();
            this.labelRaciones = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxCategoria
            // 
            this.comboBoxCategoria.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxCategoria.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCategoria.FormattingEnabled = true;
            this.comboBoxCategoria.Location = new System.Drawing.Point(76, 72);
            this.comboBoxCategoria.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBoxCategoria.Name = "comboBoxCategoria";
            this.comboBoxCategoria.Size = new System.Drawing.Size(139, 24);
            this.comboBoxCategoria.TabIndex = 3;
            this.comboBoxCategoria.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategoria_SelectedIndexChanged);
            // 
            // BoxDuracion
            // 
            this.BoxDuracion.BackColor = System.Drawing.SystemColors.Window;
            this.BoxDuracion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BoxDuracion.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BoxDuracion.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxDuracion.FormattingEnabled = true;
            this.BoxDuracion.Items.AddRange(new object[] {
            "1h",
            "2h",
            "3h",
            "4h"});
            this.BoxDuracion.Location = new System.Drawing.Point(76, 183);
            this.BoxDuracion.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BoxDuracion.Name = "BoxDuracion";
            this.BoxDuracion.Size = new System.Drawing.Size(139, 24);
            this.BoxDuracion.TabIndex = 4;
            this.BoxDuracion.TabStop = false;
            // 
            // LabActividad
            // 
            this.LabActividad.AutoSize = true;
            this.LabActividad.BackColor = System.Drawing.Color.Orange;
            this.LabActividad.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabActividad.Location = new System.Drawing.Point(71, 107);
            this.LabActividad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabActividad.Name = "LabActividad";
            this.LabActividad.Size = new System.Drawing.Size(69, 16);
            this.LabActividad.TabIndex = 5;
            this.LabActividad.Text = "Actividad";
            // 
            // LabDuracion
            // 
            this.LabDuracion.AutoSize = true;
            this.LabDuracion.BackColor = System.Drawing.Color.Orange;
            this.LabDuracion.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabDuracion.Location = new System.Drawing.Point(75, 163);
            this.LabDuracion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabDuracion.Name = "LabDuracion";
            this.LabDuracion.Size = new System.Drawing.Size(65, 16);
            this.LabDuracion.TabIndex = 6;
            this.LabDuracion.Text = "Duración";
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.GreenYellow;
            this.Guardar.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Guardar.Location = new System.Drawing.Point(74, 226);
            this.Guardar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(141, 44);
            this.Guardar.TabIndex = 7;
            this.Guardar.Text = "Guardar Actividad";
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // comboBoxCatComida
            // 
            this.comboBoxCatComida.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxCatComida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCatComida.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxCatComida.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCatComida.FormattingEnabled = true;
            this.comboBoxCatComida.Items.AddRange(new object[] {
            ""});
            this.comboBoxCatComida.Location = new System.Drawing.Point(323, 72);
            this.comboBoxCatComida.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBoxCatComida.Name = "comboBoxCatComida";
            this.comboBoxCatComida.Size = new System.Drawing.Size(141, 24);
            this.comboBoxCatComida.TabIndex = 10;
            this.comboBoxCatComida.SelectedIndexChanged += new System.EventHandler(this.comboBoxCatComida_SelectedIndexChanged);
            // 
            // comboBoxComida
            // 
            this.comboBoxComida.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxComida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComida.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxComida.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxComida.FormattingEnabled = true;
            this.comboBoxComida.Location = new System.Drawing.Point(323, 126);
            this.comboBoxComida.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBoxComida.Name = "comboBoxComida";
            this.comboBoxComida.Size = new System.Drawing.Size(141, 24);
            this.comboBoxComida.TabIndex = 12;
            this.comboBoxComida.SelectedIndexChanged += new System.EventHandler(this.comboBoxComida_SelectedIndexChanged);
            // 
            // LabComida
            // 
            this.LabComida.AutoSize = true;
            this.LabComida.BackColor = System.Drawing.Color.Orchid;
            this.LabComida.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabComida.Location = new System.Drawing.Point(320, 107);
            this.LabComida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabComida.Name = "LabComida";
            this.LabComida.Size = new System.Drawing.Size(55, 16);
            this.LabComida.TabIndex = 14;
            this.LabComida.Text = "Comida";
            // 
            // BoxActividades
            // 
            this.BoxActividades.BackColor = System.Drawing.SystemColors.Window;
            this.BoxActividades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BoxActividades.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BoxActividades.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxActividades.FormattingEnabled = true;
            this.BoxActividades.Location = new System.Drawing.Point(74, 126);
            this.BoxActividades.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BoxActividades.Name = "BoxActividades";
            this.BoxActividades.Size = new System.Drawing.Size(139, 24);
            this.BoxActividades.TabIndex = 15;
            this.BoxActividades.SelectedIndexChanged += new System.EventHandler(this.BoxActividades_SelectedIndexChanged);
            // 
            // labCategoria
            // 
            this.labCategoria.AutoSize = true;
            this.labCategoria.BackColor = System.Drawing.Color.Orange;
            this.labCategoria.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCategoria.Location = new System.Drawing.Point(73, 53);
            this.labCategoria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labCategoria.Name = "labCategoria";
            this.labCategoria.Size = new System.Drawing.Size(118, 16);
            this.labCategoria.TabIndex = 16;
            this.labCategoria.Text = "Tipo de Actividad";
            // 
            // labCatComida
            // 
            this.labCatComida.AutoSize = true;
            this.labCatComida.BackColor = System.Drawing.Color.Orchid;
            this.labCatComida.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCatComida.Location = new System.Drawing.Point(320, 53);
            this.labCatComida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labCatComida.Name = "labCatComida";
            this.labCatComida.Size = new System.Drawing.Size(110, 16);
            this.labCatComida.TabIndex = 17;
            this.labCatComida.Text = "Tipos de Comida";
            // 
            // GuardarComida
            // 
            this.GuardarComida.BackColor = System.Drawing.Color.GreenYellow;
            this.GuardarComida.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuardarComida.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GuardarComida.Location = new System.Drawing.Point(324, 226);
            this.GuardarComida.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GuardarComida.Name = "GuardarComida";
            this.GuardarComida.Size = new System.Drawing.Size(141, 44);
            this.GuardarComida.TabIndex = 19;
            this.GuardarComida.Text = "Guardar Comida";
            this.GuardarComida.UseVisualStyleBackColor = false;
            this.GuardarComida.Click += new System.EventHandler(this.GuardarComida_Click);
            // 
            // comboBoxRaciones
            // 
            this.comboBoxRaciones.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxRaciones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRaciones.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxRaciones.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxRaciones.FormattingEnabled = true;
            this.comboBoxRaciones.Location = new System.Drawing.Point(323, 183);
            this.comboBoxRaciones.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBoxRaciones.Name = "comboBoxRaciones";
            this.comboBoxRaciones.Size = new System.Drawing.Size(141, 24);
            this.comboBoxRaciones.TabIndex = 20;
            // 
            // labelRaciones
            // 
            this.labelRaciones.AutoSize = true;
            this.labelRaciones.BackColor = System.Drawing.Color.Orchid;
            this.labelRaciones.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRaciones.Location = new System.Drawing.Point(321, 163);
            this.labelRaciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRaciones.Name = "labelRaciones";
            this.labelRaciones.Size = new System.Drawing.Size(113, 16);
            this.labelRaciones.TabIndex = 21;
            this.labelRaciones.Text = "Cantidad/Ración";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Orange;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 22;
            this.label1.Text = "Actividades";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orange;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(269, 293);
            this.panel1.TabIndex = 23;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Orchid;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(267, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(267, 293);
            this.panel2.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(79, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Comidas";
            // 
            // NuevoRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(534, 293);
            this.Controls.Add(this.labelRaciones);
            this.Controls.Add(this.comboBoxRaciones);
            this.Controls.Add(this.GuardarComida);
            this.Controls.Add(this.labCatComida);
            this.Controls.Add(this.labCategoria);
            this.Controls.Add(this.BoxActividades);
            this.Controls.Add(this.LabComida);
            this.Controls.Add(this.comboBoxComida);
            this.Controls.Add(this.comboBoxCatComida);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.LabDuracion);
            this.Controls.Add(this.LabActividad);
            this.Controls.Add(this.BoxDuracion);
            this.Controls.Add(this.comboBoxCategoria);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NuevoRegistro";
            this.ShowInTaskbar = false;
            this.Text = "NuevoRegistro";
            this.Load += new System.EventHandler(this.NuevoRegistro_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LabActividad;
        private System.Windows.Forms.Label LabDuracion;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Label LabComida;
        private System.Windows.Forms.Label labCategoria;
        private System.Windows.Forms.Label labCatComida;
        private System.Windows.Forms.Button GuardarComida;
        private System.Windows.Forms.Label labelRaciones;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox comboBoxCategoria;
        public System.Windows.Forms.ComboBox BoxActividades;
        public System.Windows.Forms.ComboBox BoxDuracion;
        public System.Windows.Forms.ComboBox comboBoxCatComida;
        public System.Windows.Forms.ComboBox comboBoxComida;
        public System.Windows.Forms.ComboBox comboBoxRaciones;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
    }
}