﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Controlador;
using Tamagochi.Modelo;
using Tamagochi.Vista;
using System.Media;

namespace Tamagochi.Vista
{
    public partial class UsuarioGeneral : Form
    {
        //Declaramos un controlador
        private UsuarioController user = new UsuarioController();
        private TamagochiController tama = new TamagochiController();

        //Declaramos un id para identificar al usuario
        private int Id;
        private int usu_mal = 0;
        /*
        private bool Valida ()
        {
            foreach (Control c in groupBox1.Controls)
            {
                if (c is TextBox)
                {
                    TextBox aux = (TextBox)c;
                    if (String.IsNullOrEmpty(aux.Text.Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        */
        public UsuarioGeneral(int id = 0)
        {
            // Modificamos controlador para recibir id
            InitializeComponent();
            this.Id = id;
        }

        private void UsuarioGeneral_Load(object sender, EventArgs e)
        {
            AcceptButton = button2;
        }

        //boton de registro lleva a la vista de registro
        private void button1_Click(object sender, EventArgs e)
        {
           
            UsuarioNuevo registro = new UsuarioNuevo();
        
            registro.ShowDialog();

        }

        //boton de acceso a form principal
        private void button2_Click(object sender, EventArgs e)
        {            
            string userName = textBox1.Text;
            string password = textBox2.Text;
            // llamamos al metodo valida y miramos si los campos estan llenos dentro del panel
            bool llenos = Valida.Validatxt(panel1);
           // MessageBox.Show("intentos:" + usu_mal);       
            if (llenos == true )
            {
                // Comprueva si userName y pasword son correctos y passa el Usuario a Usuario_Actual
                Usuario.Usuario_Actual = user.Autentificar(userName, password);
                if (Usuario.Usuario_Actual != null)
                {
                    // Coge el Tamagochi con el id del Usuario y lo establece como el Tamagochi del Usario_Actual
                    Usuario.Usuario_Actual.Tamagochis = tama.GetTamaByUserId(Usuario.Usuario_Actual.Id);
                    DateTime d = Usuario.Usuario_Actual.Tamagochis.DataRegistro;
                    this.Hide();

                    TamagochiCentral a = new TamagochiCentral();
                    a.Show();
                }
                // si no devuelve ningun usuario envia un mensaje de error
                else
                {
                    usu_mal++;
                    MessageBox.Show("Usuario incorrecto o contraseña incorrecta");
                    // Al tercer intento aparece el boton
                    if (usu_mal == 3)
                    {
                        button3.Visible = true;
                    }
                }
            }
            else
            {
                MessageBox.Show("Por favor: Complete todos los campos.");
            }        
        }


       
        private void UsuarioGeneral_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Contraseña C = new Contraseña();
            C.ShowDialog();
            C.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
