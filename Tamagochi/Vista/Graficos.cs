﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Tamagochi.Vista;
using Tamagochi.Modelo;
using Tamagochi.Controlador;

namespace Tamagochi.Vista
{
    public partial class Graficos : Form
    {
        GraficoController gfcon = new GraficoController();      
        DateTime diaSelect;
        int nombreestado;
        public Graficos()
        {
            InitializeComponent();
        }

        public Graficos(DateTime dia)
        {
            InitializeComponent();
            this.diaSelect = dia;
            

        }

        public Graficos(DateTime dia, int numestado)
        {
            InitializeComponent();
            this.diaSelect = dia;
            this.nombreestado = numestado;

        }

        private void Graficos_Load(object sender, EventArgs e)
        {
            // string fechaDeCalendario = diaSelect.ToString("yyyy-MM-dd");


            chart1.Series["serie"].XValueType = ChartValueType.Date;
            //chart1.Series["Series2"].XValueType = ChartValueType.Date;

            //Grafico graf = new Grafico();

            //graf = gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, fechaDeCalendario);

            //chart1.Series["Higiene"].Points.AddXY(graf.Fecha, graf.Higiene);


            //chart1.Series["Series2"].Points.AddXY(graf.Fecha, graf.Higiene);

            //graf = gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, "2018-03-08");

            //for text1 hasta el text 2 de todos los dias
            List<Grafico> lgraf = new List<Grafico>();

            double diasPasados=(diaSelect- Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {
               // if(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")).Fecha=)
              //      {
              //      gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")).Fecha = Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i);
              //  }
                lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));
               
            }
            chart1.Series["serie"].LegendText = "Media General";
            chart1.Series["serie"].Color = Color.Lime;
            foreach (Grafico item in lgraf)
            {
                int mediageneral = (item.Higiene + item.Cansancio + item.Conocimientos + item.SaludMental + item.FormaFisica + item.Hambre) / 6;

                chart1.Series["serie"].Points.AddXY(item.Fecha, mediageneral);
            }
                //    gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, "2018-03-08");

                //date_registro

                //List < gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, dia) >


                // chart1.Series["Series1"].Points.AddXY(item.Fecha, item.FormaFisica);

                //DateTime ahora=DateTime.Now;



                // MessageBox.Show("el calendario tiene "+fechaDeCalendario);


                // chart1.Series["F.Fisica"].XValueType = ChartValueType.Date;
                // chart1.Series["Higiene"].XValueType = ChartValueType.Date;


                // foreach (Grafico item in gfcon.listgrafico(Usuario.Usuario_Actual.Tamagochis.Id, fechaDeCalendario))
                // {


                //     chart1.Series["F.Fisica"].Points.AddXY(item.Fecha, item.FormaFisica);
                //     chart1.Series["Higiene"].Points.AddXY(item.Fecha, item.Higiene);

                // }

                /*
                chart1.Series["Series1"].LegendText = "Hambre";         

                Dictionary<string, int> dic = new Dictionary<string, int>();

                List<Grafico> ResultadoGrafico = new List<Grafico>();

                ResultadoGrafico = gf.GetTodoGrafico(Usuario.Usuario_Actual.Tamagochis.Id);
                //int media = Usuario.Usuario_Actual.Tamagochis.Hambre;
                int hambre = 250;
                int cansancio = 250;        

                foreach (Grafico item in ResultadoGrafico)
                {

                chart1.Series["Series1"].XValueType = ChartValueType.Date;

                // string f = string.Format("{0}", item.Fecha);
                hambre += item.Hambre;
                cansancio += item.Cansancio;

                chart1.Series["Series1"].Points.AddXY(item.Fecha, hambre);
                }
                */

        }

        TamagochiCentral tc = new TamagochiCentral();

        private void button1_Click(object sender, EventArgs e)
        {
            List<Grafico> b1lgraf = new List<Grafico>();
            chart1.Series["serie"].LegendText = "Media General";
            chart1.Series["serie"].Color = Color.Lime;
            chart1.Series["serie"].Points.Clear();



            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {
                
                b1lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            

            foreach (Grafico item in b1lgraf)
            {
                int mediageneral = (item.Higiene + item.Cansancio + item.Conocimientos + item.SaludMental + item.FormaFisica + item.Hambre) / 6;

                chart1.Series["serie"].Points.AddXY(item.Fecha, mediageneral);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Grafico> b2lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.Red;
            chart1.Series["serie"].LegendText = "Hambre";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

                b2lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }

            foreach (Grafico item in b2lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.Hambre);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Grafico> b3lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.Yellow;
            chart1.Series["serie"].LegendText = "Cansancio";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

                b3lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            
            foreach (Grafico item in b3lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.Cansancio);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<Grafico> b4lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.Blue;
            chart1.Series["serie"].LegendText = "Higiene";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

                b4lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            
            foreach (Grafico item in b4lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.Higiene);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            List<Grafico> b5lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.Orange;
            chart1.Series["serie"].LegendText = "Forma Fisica";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

                b5lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            
            foreach (Grafico item in b5lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.FormaFisica);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<Grafico> b6lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.DarkGray;
            chart1.Series["serie"].LegendText = "Salud Mental";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

               b6lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            
            foreach (Grafico item in b6lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.SaludMental);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            List<Grafico> b7lgraf = new List<Grafico>();
            chart1.Series["serie"].Points.Clear();
            chart1.Series["serie"].Color = Color.Coral;
            chart1.Series["serie"].LegendText = "Conocimientos";
            double diasPasados = (diaSelect - Usuario.Usuario_Actual.Tamagochis.DataRegistro).TotalHours;

            diasPasados /= 24;
            int d = (int)diasPasados;

            for (int i = 0; i <= d; i++)
            {

                b7lgraf.Add(gfcon.grafico(Usuario.Usuario_Actual.Tamagochis.Id, Usuario.Usuario_Actual.Tamagochis.DataRegistro.AddDays(i).ToString("yyyy-MM-dd")));

            }
            
            foreach (Grafico item in b7lgraf)
            {

                chart1.Series["serie"].Points.AddXY(item.Fecha, item.Conocimientos);
            }
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
