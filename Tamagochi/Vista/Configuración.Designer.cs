﻿namespace Tamagochi.Vista
{
    partial class Configuración
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuración));
            this.labNuevoNombre = new System.Windows.Forms.Label();
            this.textBoxNuevoNombre = new System.Windows.Forms.TextBox();
            this.labNuevaContra = new System.Windows.Forms.Label();
            this.textBoxNuevaContra = new System.Windows.Forms.TextBox();
            this.Guardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Play = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.btn_baja = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // labNuevoNombre
            // 
            this.labNuevoNombre.AutoSize = true;
            this.labNuevoNombre.BackColor = System.Drawing.Color.Orchid;
            this.labNuevoNombre.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNuevoNombre.Location = new System.Drawing.Point(82, 44);
            this.labNuevoNombre.Name = "labNuevoNombre";
            this.labNuevoNombre.Size = new System.Drawing.Size(93, 14);
            this.labNuevoNombre.TabIndex = 0;
            this.labNuevoNombre.Text = "Nuevo Nombre";
            // 
            // textBoxNuevoNombre
            // 
            this.textBoxNuevoNombre.Location = new System.Drawing.Point(181, 41);
            this.textBoxNuevoNombre.Name = "textBoxNuevoNombre";
            this.textBoxNuevoNombre.Size = new System.Drawing.Size(142, 20);
            this.textBoxNuevoNombre.TabIndex = 1;
            // 
            // labNuevaContra
            // 
            this.labNuevaContra.AutoSize = true;
            this.labNuevaContra.BackColor = System.Drawing.Color.Orchid;
            this.labNuevaContra.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNuevaContra.Location = new System.Drawing.Point(61, 81);
            this.labNuevaContra.Name = "labNuevaContra";
            this.labNuevaContra.Size = new System.Drawing.Size(114, 14);
            this.labNuevaContra.TabIndex = 2;
            this.labNuevaContra.Text = "Nueva Contraseña";
            // 
            // textBoxNuevaContra
            // 
            this.textBoxNuevaContra.Location = new System.Drawing.Point(181, 78);
            this.textBoxNuevaContra.Name = "textBoxNuevaContra";
            this.textBoxNuevaContra.Size = new System.Drawing.Size(142, 20);
            this.textBoxNuevaContra.TabIndex = 3;
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Guardar.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Bold);
            this.Guardar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Guardar.Location = new System.Drawing.Point(353, 41);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(110, 57);
            this.Guardar.TabIndex = 4;
            this.Guardar.Text = "Editar";
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Orchid;
            this.label1.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(130, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "Sonido";
            // 
            // Play
            // 
            this.Play.BackColor = System.Drawing.Color.Orchid;
            this.Play.BackgroundImage = global::Tamagochi.Properties.Resources.von;
            this.Play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Play.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Play.FlatAppearance.BorderSize = 0;
            this.Play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Play.Location = new System.Drawing.Point(188, 128);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(37, 26);
            this.Play.TabIndex = 6;
            this.Play.UseVisualStyleBackColor = false;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // stop
            // 
            this.stop.BackColor = System.Drawing.Color.Orchid;
            this.stop.BackgroundImage = global::Tamagochi.Properties.Resources.voff1;
            this.stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stop.FlatAppearance.BorderSize = 0;
            this.stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stop.Location = new System.Drawing.Point(250, 126);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(40, 30);
            this.stop.TabIndex = 5;
            this.stop.UseVisualStyleBackColor = false;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // btn_baja
            // 
            this.btn_baja.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_baja.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Bold);
            this.btn_baja.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_baja.Image = ((System.Drawing.Image)(resources.GetObject("btn_baja.Image")));
            this.btn_baja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_baja.Location = new System.Drawing.Point(365, 224);
            this.btn_baja.Name = "btn_baja";
            this.btn_baja.Size = new System.Drawing.Size(128, 43);
            this.btn_baja.TabIndex = 9;
            this.btn_baja.Text = "Darse de baja";
            this.btn_baja.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_baja.UseVisualStyleBackColor = false;
            this.btn_baja.Click += new System.EventHandler(this.btn_baja_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orchid;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 279);
            this.panel1.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.GreenYellow;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(296, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(209, 279);
            this.panel2.TabIndex = 11;
            // 
            // Configuración
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(505, 279);
            this.Controls.Add(this.btn_baja);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.textBoxNuevaContra);
            this.Controls.Add(this.labNuevaContra);
            this.Controls.Add(this.textBoxNuevoNombre);
            this.Controls.Add(this.labNuevoNombre);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Configuración";
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.Configuración_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labNuevoNombre;
        private System.Windows.Forms.TextBox textBoxNuevoNombre;
        private System.Windows.Forms.Label labNuevaContra;
        private System.Windows.Forms.TextBox textBoxNuevaContra;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_baja;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}