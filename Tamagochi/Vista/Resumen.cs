﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Controlador;
using Tamagochi.Modelo;

namespace Tamagochi.Vista
{
    public partial class Resumen : Form
    {
        public Resumen()
        {
            InitializeComponent();
            
        }

        private void Resumen_Load(object sender, EventArgs e)
        {
            
            ComidasController comc = new ComidasController();

            labelCal.Text = comc.GetCalPerDay().Cal.ToString() + " Kcal";
            labelGrasas.Text = comc.GetCalPerDay().Grasas.ToString() + " gramos";
            labelAzucares.Text = comc.GetCalPerDay().Azucares.ToString() + " gramos";

            float v1, v2 , v3, v4 , v5 , v6 , v7;

            progressHambre.Value = Usuario.Usuario_Actual.Tamagochis.Hambre;
            v1 = ((float)progressHambre.Value) / ((float)500) * ((float)100);
            PorcientoHambre.Text = v1 + "%";

            progressCansancio.Value = Usuario.Usuario_Actual.Tamagochis.Cansancio;
            v2 = ((float)progressCansancio.Value) / ((float)500) * ((float)100);
            PorcientoCansancio.Text = v2 + "%";

            progressHigiene.Value = Usuario.Usuario_Actual.Tamagochis.Higiene;
            v3 = ((float)progressHigiene.Value) / ((float)500) * ((float)100);
            PorcientoHigiene.Text = v3 + "%";

            progressFisica.Value = Usuario.Usuario_Actual.Tamagochis.FormaFisica;
            v4= ((float)progressFisica.Value) / ((float)500) * ((float)100);
            PorcientoFF.Text = v4 + "%";

         
            progressBarConoci.Value = Usuario.Usuario_Actual.Tamagochis.Conocimientos;
            v5 = ((float)progressBarConoci.Value) / ((float)500) * ((float)100);
            PorcientoConoci.Text = v5+ "%";

            progressBarMental.Value = Usuario.Usuario_Actual.Tamagochis.SaludMental;
            v6 = ((float)progressBarMental.Value) / ((float)500) * ((float)100);
            PorcientoMental.Text = v6 + "%";

            progressMedia.Value = Usuario.Usuario_Actual.Tamagochis.MediaGeneral;
            v7 = ((float)progressMedia.Value) / ((float)500) * ((float)100);
            PorcientoMedia.Text = v7 + "%";
              
            foreach (Control pro in this.Controls)
            {
                if(pro is ProgressBar)
                {
                    if( ((ProgressBar)pro).Value > 450 || ((ProgressBar)pro).Value < 50)
                    {
                        pro.ForeColor = Color.Red;
                    }
                    else
                    {
                        pro.ForeColor = Color.Blue;

                    }
                }
            }              
        }

        private void progressHambre_Click(object sender, EventArgs e)
        {
           

        }

        private void progressHigiene_Click(object sender, EventArgs e)
        {

        }

        private void progressCansancio_Click(object sender, EventArgs e)
        {

        }

        private void progressFisica_Click(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }
    }
}
