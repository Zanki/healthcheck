﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;
using Tamagochi.Controlador;
using MySql.Data.MySqlClient;
using WMPLib;
using System.Media;
using Tamagochi.Vista;


namespace Tamagochi.Vista
{
    public partial class TamagochiCentral : Form
    {
        //Declaramos Controlador
        private UsuarioController user = new UsuarioController();
        private TamagochiController Tc = new TamagochiController();
        private TamagochiCentral tamacentral;

        List<ListaActividades> ll = new List<ListaActividades>();
        ListaActividadesController listact = new ListaActividadesController();
        List<ListaComidas> com = new List<ListaComidas>();
        ListaComidasController listeat = new ListaComidasController();

        DateTime DateStart = new DateTime();
        DateTime TiempoEjecucion = new DateTime();

        SoundPlayer sonido;
             
        public TamagochiCentral()
        {
            InitializeComponent();
        }
        
        //HOla que tal

        public void TamagochiCentral_Load(object sender, EventArgs e)
        {
            
            diasjugados();

            #region calculo del tiempo que ha pasado

            //Retorna el tiempodesde la última conexión

            double minutos = Tc.diferenciaHoras(Usuario.Usuario_Actual.Id);

            MessageBox.Show("Tiempo desde la última conexión:\n\n" + minutos);

            Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion += minutos;

            minutos = Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion;

            //MessageBox.Show("Tiempo total (minutos):\n\n" + minutos);
            //MessageBox.Show("Tiempo total:\n\n" + Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion);

            //normaliza a 5 horas
            //double resto_horas = minutos / 1;  // cada '1' minutos

            double cada = 300; // cada 300 minutos (5 horas) actualiza
            double resto_horas = minutos / cada; //cada '300' minuts (5 horas)
            int horas_bucle = (int)resto_horas;
            double horasformato5 = Math.Abs(horas_bucle * cada - minutos);

            //MessageBox.Show("horas minutos: " + minutos + "horas horas_bucle: " + horas_bucle + "\n horas formato5: " + horasformato5);

            resto_horas = Math.Abs(minutos - horasformato5);
            resto_horas = resto_horas / cada;

            //MessageBox.Show(":\n\n" + resto_horas);

            //Actualiza estados del tamagochi en funcion de las horas que han passado desde la última conexión
            if(resto_horas > 0)
            {
                for (int i = 1; i <= resto_horas; i++)
                {
                    Tc.ActualizarEstadosTamagochiUsuarioActual(Tc.restadiari());
                }
                //Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion -= resto_horas * 300;
                Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion -= horas_bucle * cada;
            }

            Tc.ActualizarHoraActualizacion(Usuario.Usuario_Actual.Tamagochis.Id);
            
            //Calcula las horas que faltan hasta la siguiente quinta hora

            //timer1.Interval = 60000;  //actualiza cada '60' segundos (1 minutos)
            timer1.Interval = 60000;  //actualiza cada '60' segundos (1 minutos)
            timer1.Start();

            #endregion

            // pictureBox1.Image = Properties.Resources.presente;
            ListaActividadesController listact = new ListaActividadesController();


            #region DataGrid's
            List<ListaActividades> ll = listact.GetAllParaHistorial();
            this.dGV.DataSource = ll;
            
            this.dGV.Columns["NombreActividad"].DisplayIndex = 0;
            this.dGV.Columns["Fecha"].DisplayIndex = 1;
            this.dGV.Columns["idTamagochi"].Visible = false;
            this.dGV.Columns["idActividades"].Visible = false;

            ListaComidasController listeat = new ListaComidasController();
            List<ListaComidas> com = listeat.GetAllHistorialComidas();
            this.dGV1.DataSource = com;

            this.dGV1.Columns["NombreComidas"].DisplayIndex = 0;
            this.dGV1.Columns["Racion"].DisplayIndex = 1;
            this.dGV1.Columns["Fecha"].DisplayIndex = 2;     
            this.dGV1.Columns["IdTamagochi"].Visible = false;
            this.dGV1.Columns["IdComidas"].Visible = false;

            //listact.Historial(dGV);
            #endregion

            #region Labels i imagen
            labusuario.Text = Usuario.Usuario_Actual.Nombre;
            labpeso.Text = Usuario.Usuario_Actual.Peso.ToString();

            labusuario.Text = Usuario.Usuario_Actual.Nombre ;
            labpeso.Text = Usuario.Usuario_Actual.Peso.ToString() + " Kg";
            labedad.Text = Usuario.Usuario_Actual.Edad.ToString();
            #endregion

            pictureBox1.Image = Properties.Resources.presente;

            #region sonido
            try
            {
                sonido = new SoundPlayer(Properties.Resources.sound);
                sonido.PlayLooping();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error"+ex);
                throw;
            }
            #endregion

        }

        public void AddFormEnPanel(object formQueQueremosAgregar)
    {
        if (this.panel1.Controls.Count > 0)
            this.panel1.Controls.RemoveAt(0);
        Form fh = formQueQueremosAgregar as Form;
        fh.TopLevel = false;
        fh.FormBorderStyle = FormBorderStyle.None;
        fh.Dock = DockStyle.Fill;
        this.panel1.Controls.Add(fh);
        this.panel1.Tag = fh;
        fh.Show();

    }

        private void actividades_Click(object sender, EventArgs e)
        {
            
            AddFormEnPanel(new NuevoRegistro(this));
            
        }

        private void salir_Click(object sender, EventArgs e)
        {
           if(MessageBox.Show("¿Seguro que desea salir?", "Salir", MessageBoxButtons.YesNo) == DialogResult.Yes)
           {
                DateTime data_salida = DateTime.Now;
               // MessageBox.Show("Data:\n\n" + data_salida);
                Tc.registrosalida(Usuario.Usuario_Actual.Id, data_salida); //Guardar TiempoHastaActualizacion
                Environment.Exit(0);
            }
            else
            {
                return;
            }
            
        }

        /* Dar de baja
        private void Dar_baja(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de dejarnos para siempre ?", "Advertencia",  MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (MessageBox.Show("Seguro seguro???", "Eliminar 2", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                {
                    if (MessageBox.Show("Ultima oportunidad!!!", "Eliminar 3", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)
                    {
                        user.borrar(Usuario.Usuario_Actual);
                        Application.Exit();
                    }
                }
            }
        }
        */

        private void TamagochiCentral_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        /*      Llista(ListaActividades lista)
        public void Llista(ListaActividades lista)
        {

            dGV.ColumnCount = 3;
            dGV.RowCount = lista.GetNum() + 1;
            int j = 0;

            dGV[0, 0].Value = "IdTamagochi";
            dGV[1, 0].Value = "IdActividad";
            dGV[2, 0].Value = "Data";

            foreach (Actividades act in lista.actividades)
            {
                ListaActividades act = new ListaActividades();
                dGV[0, j + 1].Value = act.GetIdTanagochi();
                dGV[1, j + 1].Value = act.GetIdActividad();
                dGV[2, j + 1].Value = act.GetFecha();
            }

            while (j < lista.GetNum())
            {
                ListaActividades act = new ListaActividades();
                act = lista.ConsultarLista(j);
                dGV[0, j + 1].Value = act.GetIdTanagochi();
                dGV[1, j + 1].Value = act.GetIdActividad();
                dGV[2, j + 1].Value = act.GetFecha();

                j++;
            }
        }
        */

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonResumen_Click(object sender, EventArgs e)
        {
           
            AddFormEnPanel(new Resumen());
        }

        private void labusuario_Click(object sender, EventArgs e)
        {

        }

        private void labpeso_Click(object sender, EventArgs e)
        {

        }

        private void Config_Click(object sender, EventArgs e)
        {
            AddFormEnPanel(new Configuración());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddFormEnPanel(new Graficos(monthCalendar1.SelectionStart));
        }

        private void plandevida_Click(object sender, EventArgs e)
        {
            Premium pre = new Premium();
            pre.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Consejos F = new Consejos();
            F.ShowDialog();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            pictureBox1.Image = Properties.Resources.presente;
        }

        public void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            string dataform = this.monthCalendar1.SelectionRange.Start.ToString("yyyy-MM-dd");
            // string dataform2 = this.monthCalendar1.Selection.ToString("yyyy-MM-dd");
            
            MessageBox.Show(dataform);

            ll = listact.GetDiaParaHistorial(dataform);
            com = listeat.GetDiaParaHistorialComidas(dataform);
            this.dGV.DataSource = ll;
            this.dGV1.DataSource = com;
            AddFormEnPanel(new Graficos(monthCalendar1.SelectionStart));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion += 1;  //cada Interval suma '1' minutos
            if (Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion > 300) //si tiempoActualizacion > '300' minutos actualiza (5horas)
            {
                diasjugados();
                Tc.ActualizarEstadosTamagochiUsuarioActual(Tc.restadiari());
                Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion = 0;
            }
        }

        private void dGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dGV.CurrentCell.RowIndex;
            string SelectedText = Convert.ToString(dGV.Rows[index].Cells[4].FormattedValue.ToString());
            string stringidfila = dGV.Rows[index].Cells[2].Value.ToString();
            int duracion = (int)dGV.Rows[index].Cells[3].Value;

            ActividadesController ac = new ActividadesController();

            NuevoRegistro n = new NuevoRegistro(this);
            AddFormEnPanel(n);

            if (!n.BoxActividades.Enabled) n.BoxActividades.Enabled = true;

            Actividades act = new Actividades();
            act = ac.GetActividadByName(SelectedText);

            n.comboBoxCategoria.SelectedItem = n.comboBoxCategoria.Items[act.categoria - 1];
            n.BoxActividades.SelectedIndex = n.BoxActividades.FindStringExact(SelectedText);

            if (duracion != 0)
            {
                if (!n.BoxDuracion.Enabled) n.BoxDuracion.Enabled = true;
                if(act.categoria == 7)
                {
                    n.BoxDuracion.SelectedIndex = n.BoxDuracion.FindStringExact(10 * duracion + " minutos");
                }
                else
                {
                    n.BoxDuracion.SelectedIndex = n.BoxDuracion.FindStringExact(duracion + " horas");
                }
            }
            //n.BoxActividades.SelectedValue = n.BoxActividades.Items[SelectedText];
            //n.comboBoxCategoria.SelectedValue = act.categoria.ToString();
            //n.BoxActividades.SelectedValue = stringidfila;
        }

        private void TamagochiCentral_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if(MessageBox.Show("¿Seguro que desea salir?", "Salir", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //    {
                    DateTime data_salida = DateTime.Now;
                    MessageBox.Show("Data:\n\n" + data_salida);
                    Tc.registrosalida(Usuario.Usuario_Actual.Id, data_salida); //Guardar TiempoHastaActualizacion
                    Application.Exit();
                //}
            //else
            //    {
            //        return;
            //    }
        }

        private void diasjugados()
        {
            DateTime dataregistro=Usuario.Usuario_Actual.Tamagochis.DataRegistro; 
            double dj = DateTime.Now.Subtract(dataregistro).TotalDays;
            int d = (int)dj;
            label4.Text = d.ToString();
        }

        private void dGV1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dGV1.CurrentCell.RowIndex;
            string SelectedText = Convert.ToString(dGV1.Rows[index].Cells[4].FormattedValue.ToString());
            string stringidfila = dGV1.Rows[index].Cells[3].Value.ToString();
            int racion = (int)dGV1.Rows[index].Cells[1].Value;

            ComidasController comc = new ComidasController();

            NuevoRegistro n = new NuevoRegistro(this);
            AddFormEnPanel(n);

            if (!n.comboBoxComida.Enabled) n.comboBoxComida.Enabled = true;

            Comidas com = new Comidas();
            com = comc.GetValuesFromComida(SelectedText);

            n.comboBoxCatComida.SelectedItem = n.comboBoxCatComida.Items[com.IdCategoriasComidas - 1];
            n.comboBoxComida.SelectedIndex = n.comboBoxComida.FindStringExact(SelectedText);

            if (racion != 0)
            {
                if (!n.comboBoxRaciones.Enabled) n.comboBoxRaciones.Enabled = true;
                n.comboBoxRaciones.SelectedIndex = n.comboBoxRaciones.FindStringExact("" + racion);
            }

            //n.BoxActividades.SelectedValue = n.BoxActividades.Items[SelectedText];
            //n.comboBoxCategoria.SelectedValue = act.categoria.ToString();
            //n.BoxActividades.SelectedValue = stringidfila;
        }

        private void panel2_Paint_1(object sender, PaintEventArgs e)
        {

        }
    }
}
