﻿namespace Tamagochi.Vista
{
    partial class Resumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Resumen));
            this.labelHambre = new System.Windows.Forms.Label();
            this.labelHigiene = new System.Windows.Forms.Label();
            this.labelCansancio = new System.Windows.Forms.Label();
            this.labelFormaFisica = new System.Windows.Forms.Label();
            this.labelMediaGeneral = new System.Windows.Forms.Label();
            this.progressMedia = new System.Windows.Forms.ProgressBar();
            this.progressHambre = new System.Windows.Forms.ProgressBar();
            this.progressCansancio = new System.Windows.Forms.ProgressBar();
            this.progressHigiene = new System.Windows.Forms.ProgressBar();
            this.progressFisica = new System.Windows.Forms.ProgressBar();
            this.labelSalMental = new System.Windows.Forms.Label();
            this.progressBarMental = new System.Windows.Forms.ProgressBar();
            this.labelConoci = new System.Windows.Forms.Label();
            this.progressBarConoci = new System.Windows.Forms.ProgressBar();
            this.PorcientoFF = new System.Windows.Forms.Label();
            this.PorcientoMental = new System.Windows.Forms.Label();
            this.PorcientoConoci = new System.Windows.Forms.Label();
            this.PorcientoHambre = new System.Windows.Forms.Label();
            this.PorcientoCansancio = new System.Windows.Forms.Label();
            this.PorcientoHigiene = new System.Windows.Forms.Label();
            this.PorcientoMedia = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.labelCantidadesIngeridasInfo = new System.Windows.Forms.Label();
            this.labelCalTexto = new System.Windows.Forms.Label();
            this.labelCal = new System.Windows.Forms.Label();
            this.labelGrasasText = new System.Windows.Forms.Label();
            this.labelGrasas = new System.Windows.Forms.Label();
            this.labelAzucaresText = new System.Windows.Forms.Label();
            this.labelAzucares = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // labelHambre
            // 
            this.labelHambre.AutoSize = true;
            this.labelHambre.BackColor = System.Drawing.Color.Coral;
            this.labelHambre.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHambre.Location = new System.Drawing.Point(31, 31);
            this.labelHambre.Name = "labelHambre";
            this.labelHambre.Size = new System.Drawing.Size(58, 14);
            this.labelHambre.TabIndex = 3;
            this.labelHambre.Text = "Saciedad";
            // 
            // labelHigiene
            // 
            this.labelHigiene.AutoSize = true;
            this.labelHigiene.BackColor = System.Drawing.Color.Coral;
            this.labelHigiene.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHigiene.Location = new System.Drawing.Point(31, 97);
            this.labelHigiene.Name = "labelHigiene";
            this.labelHigiene.Size = new System.Drawing.Size(52, 14);
            this.labelHigiene.TabIndex = 4;
            this.labelHigiene.Text = "Higiene";
            // 
            // labelCansancio
            // 
            this.labelCansancio.AutoSize = true;
            this.labelCansancio.BackColor = System.Drawing.Color.Coral;
            this.labelCansancio.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCansancio.Location = new System.Drawing.Point(31, 62);
            this.labelCansancio.Name = "labelCansancio";
            this.labelCansancio.Size = new System.Drawing.Size(58, 14);
            this.labelCansancio.TabIndex = 5;
            this.labelCansancio.Text = "Descanso";
            // 
            // labelFormaFisica
            // 
            this.labelFormaFisica.AutoSize = true;
            this.labelFormaFisica.BackColor = System.Drawing.Color.Coral;
            this.labelFormaFisica.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFormaFisica.Location = new System.Drawing.Point(267, 31);
            this.labelFormaFisica.Name = "labelFormaFisica";
            this.labelFormaFisica.Size = new System.Drawing.Size(81, 14);
            this.labelFormaFisica.TabIndex = 6;
            this.labelFormaFisica.Text = "Forma Física";
            // 
            // labelMediaGeneral
            // 
            this.labelMediaGeneral.AutoSize = true;
            this.labelMediaGeneral.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.labelMediaGeneral.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMediaGeneral.Location = new System.Drawing.Point(31, 181);
            this.labelMediaGeneral.Name = "labelMediaGeneral";
            this.labelMediaGeneral.Size = new System.Drawing.Size(90, 14);
            this.labelMediaGeneral.TabIndex = 7;
            this.labelMediaGeneral.Text = "Media General";
            // 
            // progressMedia
            // 
            this.progressMedia.Location = new System.Drawing.Point(34, 208);
            this.progressMedia.Maximum = 500;
            this.progressMedia.Name = "progressMedia";
            this.progressMedia.Size = new System.Drawing.Size(420, 13);
            this.progressMedia.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressMedia.TabIndex = 11;
            // 
            // progressHambre
            // 
            this.progressHambre.BackColor = System.Drawing.SystemColors.Control;
            this.progressHambre.Location = new System.Drawing.Point(94, 31);
            this.progressHambre.Maximum = 500;
            this.progressHambre.Name = "progressHambre";
            this.progressHambre.Size = new System.Drawing.Size(100, 13);
            this.progressHambre.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressHambre.TabIndex = 12;
            this.progressHambre.Click += new System.EventHandler(this.progressHambre_Click);
            // 
            // progressCansancio
            // 
            this.progressCansancio.Cursor = System.Windows.Forms.Cursors.Default;
            this.progressCansancio.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.progressCansancio.Location = new System.Drawing.Point(94, 62);
            this.progressCansancio.Maximum = 500;
            this.progressCansancio.Name = "progressCansancio";
            this.progressCansancio.Size = new System.Drawing.Size(100, 13);
            this.progressCansancio.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressCansancio.TabIndex = 13;
            this.progressCansancio.Click += new System.EventHandler(this.progressCansancio_Click);
            // 
            // progressHigiene
            // 
            this.progressHigiene.Location = new System.Drawing.Point(94, 97);
            this.progressHigiene.Maximum = 500;
            this.progressHigiene.Name = "progressHigiene";
            this.progressHigiene.Size = new System.Drawing.Size(100, 13);
            this.progressHigiene.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressHigiene.TabIndex = 14;
            this.progressHigiene.Click += new System.EventHandler(this.progressHigiene_Click);
            // 
            // progressFisica
            // 
            this.progressFisica.Cursor = System.Windows.Forms.Cursors.Default;
            this.progressFisica.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.progressFisica.Location = new System.Drawing.Point(354, 31);
            this.progressFisica.Maximum = 500;
            this.progressFisica.Name = "progressFisica";
            this.progressFisica.Size = new System.Drawing.Size(100, 13);
            this.progressFisica.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressFisica.TabIndex = 15;
            this.progressFisica.Click += new System.EventHandler(this.progressFisica_Click);
            // 
            // labelSalMental
            // 
            this.labelSalMental.AutoSize = true;
            this.labelSalMental.BackColor = System.Drawing.Color.Coral;
            this.labelSalMental.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSalMental.Location = new System.Drawing.Point(266, 62);
            this.labelSalMental.Name = "labelSalMental";
            this.labelSalMental.Size = new System.Drawing.Size(83, 14);
            this.labelSalMental.TabIndex = 16;
            this.labelSalMental.Text = "Salud Mental";
            // 
            // progressBarMental
            // 
            this.progressBarMental.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.progressBarMental.Location = new System.Drawing.Point(354, 62);
            this.progressBarMental.Maximum = 500;
            this.progressBarMental.Name = "progressBarMental";
            this.progressBarMental.Size = new System.Drawing.Size(100, 13);
            this.progressBarMental.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarMental.TabIndex = 17;
            // 
            // labelConoci
            // 
            this.labelConoci.AutoSize = true;
            this.labelConoci.BackColor = System.Drawing.Color.Coral;
            this.labelConoci.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConoci.Location = new System.Drawing.Point(259, 97);
            this.labelConoci.Name = "labelConoci";
            this.labelConoci.Size = new System.Drawing.Size(91, 14);
            this.labelConoci.TabIndex = 18;
            this.labelConoci.Text = "Conocimientos";
            // 
            // progressBarConoci
            // 
            this.progressBarConoci.Location = new System.Drawing.Point(354, 97);
            this.progressBarConoci.Maximum = 500;
            this.progressBarConoci.Name = "progressBarConoci";
            this.progressBarConoci.Size = new System.Drawing.Size(100, 13);
            this.progressBarConoci.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarConoci.TabIndex = 19;
            // 
            // PorcientoFF
            // 
            this.PorcientoFF.AutoSize = true;
            this.PorcientoFF.BackColor = System.Drawing.Color.Coral;
            this.PorcientoFF.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoFF.Location = new System.Drawing.Point(460, 31);
            this.PorcientoFF.Name = "PorcientoFF";
            this.PorcientoFF.Size = new System.Drawing.Size(42, 14);
            this.PorcientoFF.TabIndex = 20;
            this.PorcientoFF.Text = "label1";
            // 
            // PorcientoMental
            // 
            this.PorcientoMental.AutoSize = true;
            this.PorcientoMental.BackColor = System.Drawing.Color.Coral;
            this.PorcientoMental.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoMental.Location = new System.Drawing.Point(460, 62);
            this.PorcientoMental.Name = "PorcientoMental";
            this.PorcientoMental.Size = new System.Drawing.Size(42, 14);
            this.PorcientoMental.TabIndex = 21;
            this.PorcientoMental.Text = "label1";
            // 
            // PorcientoConoci
            // 
            this.PorcientoConoci.AutoSize = true;
            this.PorcientoConoci.BackColor = System.Drawing.Color.Coral;
            this.PorcientoConoci.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoConoci.Location = new System.Drawing.Point(460, 97);
            this.PorcientoConoci.Name = "PorcientoConoci";
            this.PorcientoConoci.Size = new System.Drawing.Size(42, 14);
            this.PorcientoConoci.TabIndex = 22;
            this.PorcientoConoci.Text = "label1";
            // 
            // PorcientoHambre
            // 
            this.PorcientoHambre.AutoSize = true;
            this.PorcientoHambre.BackColor = System.Drawing.Color.Coral;
            this.PorcientoHambre.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoHambre.Location = new System.Drawing.Point(200, 31);
            this.PorcientoHambre.Name = "PorcientoHambre";
            this.PorcientoHambre.Size = new System.Drawing.Size(42, 14);
            this.PorcientoHambre.TabIndex = 23;
            this.PorcientoHambre.Text = "label1";
            // 
            // PorcientoCansancio
            // 
            this.PorcientoCansancio.AutoSize = true;
            this.PorcientoCansancio.BackColor = System.Drawing.Color.Coral;
            this.PorcientoCansancio.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoCansancio.Location = new System.Drawing.Point(200, 62);
            this.PorcientoCansancio.Name = "PorcientoCansancio";
            this.PorcientoCansancio.Size = new System.Drawing.Size(42, 14);
            this.PorcientoCansancio.TabIndex = 24;
            this.PorcientoCansancio.Text = "label1";
            // 
            // PorcientoHigiene
            // 
            this.PorcientoHigiene.AutoSize = true;
            this.PorcientoHigiene.BackColor = System.Drawing.Color.Coral;
            this.PorcientoHigiene.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoHigiene.Location = new System.Drawing.Point(200, 97);
            this.PorcientoHigiene.Name = "PorcientoHigiene";
            this.PorcientoHigiene.Size = new System.Drawing.Size(42, 14);
            this.PorcientoHigiene.TabIndex = 25;
            this.PorcientoHigiene.Text = "label1";
            // 
            // PorcientoMedia
            // 
            this.PorcientoMedia.AutoSize = true;
            this.PorcientoMedia.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.PorcientoMedia.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PorcientoMedia.Location = new System.Drawing.Point(460, 208);
            this.PorcientoMedia.Name = "PorcientoMedia";
            this.PorcientoMedia.Size = new System.Drawing.Size(42, 14);
            this.PorcientoMedia.TabIndex = 26;
            this.PorcientoMedia.Text = "label1";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // labelCantidadesIngeridasInfo
            // 
            this.labelCantidadesIngeridasInfo.AutoSize = true;
            this.labelCantidadesIngeridasInfo.BackColor = System.Drawing.Color.Coral;
            this.labelCantidadesIngeridasInfo.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCantidadesIngeridasInfo.Location = new System.Drawing.Point(31, 127);
            this.labelCantidadesIngeridasInfo.Name = "labelCantidadesIngeridasInfo";
            this.labelCantidadesIngeridasInfo.Size = new System.Drawing.Size(207, 16);
            this.labelCantidadesIngeridasInfo.TabIndex = 27;
            this.labelCantidadesIngeridasInfo.Text = "Cantidades ingeridas durante el día:";
            // 
            // labelCalTexto
            // 
            this.labelCalTexto.AutoSize = true;
            this.labelCalTexto.BackColor = System.Drawing.Color.Coral;
            this.labelCalTexto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCalTexto.Location = new System.Drawing.Point(252, 127);
            this.labelCalTexto.Margin = new System.Windows.Forms.Padding(3);
            this.labelCalTexto.Name = "labelCalTexto";
            this.labelCalTexto.Size = new System.Drawing.Size(54, 16);
            this.labelCalTexto.TabIndex = 28;
            this.labelCalTexto.Text = "Calorias:";
            // 
            // labelCal
            // 
            this.labelCal.AutoSize = true;
            this.labelCal.BackColor = System.Drawing.Color.Coral;
            this.labelCal.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCal.Location = new System.Drawing.Point(305, 127);
            this.labelCal.Margin = new System.Windows.Forms.Padding(3);
            this.labelCal.Name = "labelCal";
            this.labelCal.Size = new System.Drawing.Size(75, 16);
            this.labelCal.TabIndex = 29;
            this.labelCal.Text = "numCalorias";
            // 
            // labelGrasasText
            // 
            this.labelGrasasText.AutoSize = true;
            this.labelGrasasText.BackColor = System.Drawing.Color.Coral;
            this.labelGrasasText.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGrasasText.Location = new System.Drawing.Point(256, 146);
            this.labelGrasasText.Margin = new System.Windows.Forms.Padding(3);
            this.labelGrasasText.Name = "labelGrasasText";
            this.labelGrasasText.Size = new System.Drawing.Size(48, 16);
            this.labelGrasasText.TabIndex = 30;
            this.labelGrasasText.Text = "Grasas:";
            // 
            // labelGrasas
            // 
            this.labelGrasas.AutoSize = true;
            this.labelGrasas.BackColor = System.Drawing.Color.Coral;
            this.labelGrasas.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGrasas.Location = new System.Drawing.Point(305, 146);
            this.labelGrasas.Margin = new System.Windows.Forms.Padding(3);
            this.labelGrasas.Name = "labelGrasas";
            this.labelGrasas.Size = new System.Drawing.Size(69, 16);
            this.labelGrasas.TabIndex = 31;
            this.labelGrasas.Text = "numGrasas";
            // 
            // labelAzucaresText
            // 
            this.labelAzucaresText.AutoSize = true;
            this.labelAzucaresText.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.labelAzucaresText.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAzucaresText.Location = new System.Drawing.Point(245, 165);
            this.labelAzucaresText.Margin = new System.Windows.Forms.Padding(3);
            this.labelAzucaresText.Name = "labelAzucaresText";
            this.labelAzucaresText.Size = new System.Drawing.Size(59, 16);
            this.labelAzucaresText.TabIndex = 32;
            this.labelAzucaresText.Text = "Azucares:";
            // 
            // labelAzucares
            // 
            this.labelAzucares.AutoSize = true;
            this.labelAzucares.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.labelAzucares.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAzucares.Location = new System.Drawing.Point(305, 165);
            this.labelAzucares.Margin = new System.Windows.Forms.Padding(3);
            this.labelAzucares.Name = "labelAzucares";
            this.labelAzucares.Size = new System.Drawing.Size(80, 16);
            this.labelAzucares.TabIndex = 33;
            this.labelAzucares.Text = "numAzucares";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Coral;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 164);
            this.panel1.TabIndex = 34;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 114);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(518, 140);
            this.panel2.TabIndex = 35;
            // 
            // Resumen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(518, 254);
            this.Controls.Add(this.labelAzucares);
            this.Controls.Add(this.labelAzucaresText);
            this.Controls.Add(this.labelGrasas);
            this.Controls.Add(this.labelGrasasText);
            this.Controls.Add(this.labelCal);
            this.Controls.Add(this.labelCalTexto);
            this.Controls.Add(this.labelCantidadesIngeridasInfo);
            this.Controls.Add(this.PorcientoMedia);
            this.Controls.Add(this.PorcientoHigiene);
            this.Controls.Add(this.PorcientoCansancio);
            this.Controls.Add(this.PorcientoHambre);
            this.Controls.Add(this.PorcientoConoci);
            this.Controls.Add(this.PorcientoMental);
            this.Controls.Add(this.PorcientoFF);
            this.Controls.Add(this.progressBarConoci);
            this.Controls.Add(this.labelConoci);
            this.Controls.Add(this.progressBarMental);
            this.Controls.Add(this.labelSalMental);
            this.Controls.Add(this.progressFisica);
            this.Controls.Add(this.progressHigiene);
            this.Controls.Add(this.progressCansancio);
            this.Controls.Add(this.progressHambre);
            this.Controls.Add(this.progressMedia);
            this.Controls.Add(this.labelMediaGeneral);
            this.Controls.Add(this.labelFormaFisica);
            this.Controls.Add(this.labelCansancio);
            this.Controls.Add(this.labelHigiene);
            this.Controls.Add(this.labelHambre);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Resumen";
            this.Text = "Resumen";
            this.Load += new System.EventHandler(this.Resumen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelHambre;
        private System.Windows.Forms.Label labelHigiene;
        private System.Windows.Forms.Label labelCansancio;
        private System.Windows.Forms.Label labelFormaFisica;
        private System.Windows.Forms.Label labelMediaGeneral;
        private System.Windows.Forms.ProgressBar progressMedia;
        private System.Windows.Forms.ProgressBar progressHambre;
        private System.Windows.Forms.ProgressBar progressCansancio;
        private System.Windows.Forms.ProgressBar progressHigiene;
        private System.Windows.Forms.Label labelSalMental;
        private System.Windows.Forms.ProgressBar progressBarMental;
        private System.Windows.Forms.Label labelConoci;
        private System.Windows.Forms.ProgressBar progressBarConoci;
        private System.Windows.Forms.Label PorcientoFF;
        private System.Windows.Forms.Label PorcientoMental;
        private System.Windows.Forms.Label PorcientoConoci;
        private System.Windows.Forms.Label PorcientoHambre;
        private System.Windows.Forms.Label PorcientoCansancio;
        private System.Windows.Forms.Label PorcientoHigiene;
        private System.Windows.Forms.Label PorcientoMedia;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressFisica;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label labelCantidadesIngeridasInfo;
        private System.Windows.Forms.Label labelCalTexto;
        private System.Windows.Forms.Label labelCal;
        private System.Windows.Forms.Label labelGrasasText;
        private System.Windows.Forms.Label labelGrasas;
        private System.Windows.Forms.Label labelAzucaresText;
        private System.Windows.Forms.Label labelAzucares;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}