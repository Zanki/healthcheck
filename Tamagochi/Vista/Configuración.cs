﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;
using Tamagochi.Controlador;
using System.Media;

namespace Tamagochi.Vista
{
    public partial class Configuración : Form
    {
        private UsuarioController user = new UsuarioController();
        SoundPlayer b = new SoundPlayer(Properties.Resources.sound);
        bool musica;

        public Configuración()
        {
            InitializeComponent();
        }


        private void Configuración_Load(object sender, EventArgs e)
        {

        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            Usuario nuevo = new Usuario();
            string pass = textBoxNuevaContra.Text;


            nuevo = Usuario.Usuario_Actual;
            Usuario.Usuario_Actual.Nombre = textBoxNuevoNombre.Text ;
            Usuario.Usuario_Actual.Contrasenya = pass;

            if(textBoxNuevoNombre.Text != "")
            {
                if(pass != "")
                {
                    user.Save2(nuevo, pass);
                    MessageBox.Show("Guardado con exito");
                }
                else
                {
                    MessageBox.Show("Introduzca una contraseña.");
                }
            }
            else
            {
                MessageBox.Show("Introduzca un nombre.");
            }

        }

        private void stop_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (b != null)
                {
                    b.Stop();
                    musica = false;
                }              
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                throw;
            }
        }

        private void Play_Click(object sender, EventArgs e)
        {      
            try
            {
                if (!musica)
                {                   
                    b.PlayLooping();
                    musica = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
                throw;
            }
        }

        private void btn_baja_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Estas seguro de dejarnos para siempre ?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (MessageBox.Show("Seguro seguro???", "Eliminar 2", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                {
                    if (MessageBox.Show("Ultima oportunidad!!!", "Eliminar 3", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)
                    {
                        user.borrar(Usuario.Usuario_Actual);
                        Application.Exit();

                    }
                }
            }
        }
    }
}
