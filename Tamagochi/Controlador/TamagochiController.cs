﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamagochi.Modelo;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Tamagochi.Controlador
{
    public class TamagochiController
    {
         private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";

        // Retorna el tamagochi que tiene el Usuario
        public Modelo.Tamagochi GetTamaByUserId(int id /*Le pasamos el id del Usuario*/)
        {
            Modelo.Tamagochi t = new Modelo.Tamagochi();
            MySqlConnection _conn = new MySqlConnection(connStr);

            try
            {

                _conn.Open();
                string sql = string.Format("SELECT * FROM  Tamagochi where Usuario_idUsuario=" + id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    t.Id = (int)rdr["idTamagochi"];
                    t.Hambre = (int)rdr["Hambre"];
                    t.Cansancio = (int)rdr["Cansancio"];
                    t.Higiene = (int)rdr["Higiene"];
                    t.FormaFisica = (int)rdr["FormaFisica"];
                    t.SaludMental = (int)rdr["SaludMental"];
                    t.Conocimientos = (int)rdr["Conocimientos"];
                    t.MediaGeneral = (int)rdr["MediaGeneral"];
                    t.DataRegistro = (DateTime)rdr["dataRegistro"];
                    t.TiempoHastaActualizacion = (double)rdr["dataHastaActualizacion"];
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return t;
        }

        // Retorna el Tamagochi con el valor de todos los estados actuales del Tamagochi
        public Modelo.Tamagochi GetAll()
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            Modelo.Tamagochi t = new Modelo.Tamagochi();

            try
            {

                _conn.Open();
                string sql = string.Format("SELECT * FROM  Tamagochi");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    t.Id = (int)rdr["idTamagochi"];
                    t.Hambre = (int)rdr["saciedad"];
                    t.Cansancio = (int)rdr["cansancio"];
                    t.Higiene = (int)rdr["higiene"];
                    t.FormaFisica = (int)rdr["fromafisica"];
                    t.SaludMental = (int)rdr["saludmental"];
                    t.Conocimientos = (int)rdr["conocimiento"];
                }

                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return t;
        }

        // Crea Tamagochi con valores iniciales a 250 con relacion al usuario con id = u.Id
        public int CreateTamagochi(int id)
        {
            Modelo.Tamagochi t = new Modelo.Tamagochi();
            MySqlConnection _conn = new MySqlConnection(connStr);
            int respuesta = 0;

            try
            {
                _conn.Open();
                string sql = "INSERT INTO Tamagochi (Hambre, Cansancio, Higiene, FormaFisica, SaludMental, Conocimientos, MediaGeneral, Usuario_idUsuario, FranjaEdad_idFranjaEdad, dataRegistro, dataHastaActualizacion) " +
                    "VALUES (@hambre, @cansancio, @higiene, @formafisica, @saludmental, @conocimiento, @mediageneral, @idusuario, @franjaedad, @dataRegistro, @datahastaact)";

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@hambre", 250);
                cmd.Parameters.AddWithValue("@cansancio", 250);
                cmd.Parameters.AddWithValue("@higiene", 250);
                cmd.Parameters.AddWithValue("@formafisica", 250);
                cmd.Parameters.AddWithValue("@saludmental", 250);
                cmd.Parameters.AddWithValue("@conocimiento", 250);
                cmd.Parameters.AddWithValue("@mediageneral", 250);
                cmd.Parameters.AddWithValue("@idusuario", id);
                cmd.Parameters.AddWithValue("@franjaedad", 1);
                cmd.Parameters.AddWithValue("@dataRegistro", DateTime.Now);
                cmd.Parameters.AddWithValue("@datahastaact", 0);

                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        // UPDATE en la base de datos de los valores del tamagochi con id = t.Id
        public int SetValues(Modelo.Tamagochi t)
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            int respuesta = 0;

            try
            {
                _conn.Open();
                //Canviar INSERT por el UPDATE
                string sql = string.Format(
                    "UPDATE Tamagochi SET Hambre = @hambre, Cansancio = @cansancio, Higiene=@higiene, FormaFisica=@formafisica, SaludMental=@saludmental, Conocimientos=@conocimiento, MediaGeneral=@mediageneral WHERE idTamagochi={0}", t.Id);

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@hambre", t.Hambre);
                cmd.Parameters.AddWithValue("@cansancio", t.Cansancio);
                cmd.Parameters.AddWithValue("@higiene", t.Higiene);
                cmd.Parameters.AddWithValue("@formafisica", t.FormaFisica);
                cmd.Parameters.AddWithValue("@saludmental", t.SaludMental);
                cmd.Parameters.AddWithValue("@conocimiento", t.Conocimientos);
                cmd.Parameters.AddWithValue("@mediageneral", t.MediaGeneral);

                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        // Actualiza los valores de estado con la actividad X en el tamagochi Usuario_Actual.Tamagochis
        public void ActualizarEstadosTamagochiUsuarioActual(Actividades act)
        {
            //Suma valores de actividad en el estado
            Usuario.Usuario_Actual.Tamagochis.Hambre += act.saciedad;
            Usuario.Usuario_Actual.Tamagochis.Cansancio += act.cansancio;
            Usuario.Usuario_Actual.Tamagochis.Higiene += act.higiene;
            Usuario.Usuario_Actual.Tamagochis.FormaFisica += act.fisico;
            Usuario.Usuario_Actual.Tamagochis.SaludMental += act.mental;
            Usuario.Usuario_Actual.Tamagochis.Conocimientos += act.conocimiento;

            #region if >499 establece a 499.
            if (Usuario.Usuario_Actual.Tamagochis.Hambre > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.Hambre = 499;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Cansancio > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.Cansancio = 499;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Higiene > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.Higiene = 499;
            }
            if (Usuario.Usuario_Actual.Tamagochis.FormaFisica > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.FormaFisica = 499;
            }
            if (Usuario.Usuario_Actual.Tamagochis.SaludMental > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.SaludMental = 499;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Conocimientos > 499)
            {
                Usuario.Usuario_Actual.Tamagochis.Conocimientos = 499;
            }
            #endregion

            #region if <1 establece a 1.
            if (Usuario.Usuario_Actual.Tamagochis.Hambre < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.Hambre = 1;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Cansancio < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.Cansancio = 1;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Higiene < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.Higiene = 1;
            }
            if (Usuario.Usuario_Actual.Tamagochis.FormaFisica < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.FormaFisica = 1;
            }
            if (Usuario.Usuario_Actual.Tamagochis.SaludMental < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.SaludMental = 1;
            }
            if (Usuario.Usuario_Actual.Tamagochis.Conocimientos < 1)
            {
                Usuario.Usuario_Actual.Tamagochis.Conocimientos = 1;
            }
            #endregion

            //Media General
            Usuario.Usuario_Actual.Tamagochis.MediaGeneral =
                (Usuario.Usuario_Actual.Tamagochis.Hambre +
                Usuario.Usuario_Actual.Tamagochis.Cansancio +
                Usuario.Usuario_Actual.Tamagochis.Higiene +
                Usuario.Usuario_Actual.Tamagochis.FormaFisica +
                Usuario.Usuario_Actual.Tamagochis.SaludMental +
                Usuario.Usuario_Actual.Tamagochis.Conocimientos) / 6;

            //Actualiza datos en la base de datos
            this.SetValues(Usuario.Usuario_Actual.Tamagochis);
        }


        #region Avisos
        public void Avisos()
        {
            this.ValoresAvisos("cansancio", Usuario.Usuario_Actual.Tamagochis.Cansancio, 50, 450);
            this.ValoresAvisos("forma física", Usuario.Usuario_Actual.Tamagochis.FormaFisica, 50, 450);
            this.ValoresAvisos("hambre", Usuario.Usuario_Actual.Tamagochis.Hambre, 50, 450);
            this.ValoresAvisos("higiene", Usuario.Usuario_Actual.Tamagochis.Higiene, 50, 450);
            this.ValoresAvisos("salud mental", Usuario.Usuario_Actual.Tamagochis.SaludMental, 50, 450);
            this.ValoresAvisos("conocimientos", Usuario.Usuario_Actual.Tamagochis.Conocimientos, 50, 450);
        }

        //rangos de valores de Categorias
        public bool ValoresAvisos(string cat, int numberToCheck, int bottom, int top)
        {         
            if (numberToCheck > top)
            {
                MessageBox.Show("Está usted por encima del limite en " + cat + ".", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);             
            }
            if (numberToCheck < bottom)
            {
                MessageBox.Show("Está usted por debajo del limite en " + cat + ".", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }          
            return (numberToCheck >= bottom && numberToCheck <= top);
        }


        public void AvisosComida()
        {
            ComidasController comc = new ComidasController();
            Comidas comida = new Comidas();

            comida = comc.GetCalPerDay();

            double limgrasas, limazucares;

            limgrasas = 30*comida.Cal/100;
            limazucares = 10*comida.Cal/100;

            this.ValoresComidaAvisos("calorias", comida.Cal, 0, 2000); //cal recomendadas 2000
            this.ValoresComidaAvisos("grasas física", comida.Grasas, 0, limgrasas);
            this.ValoresComidaAvisos("azucares", comida.Azucares, 0, limazucares);
        }

        public bool ValoresComidaAvisos(string cat, double numberToCheck, int bottom, double top)
        {

            if (numberToCheck > top)
            {
                if (cat == "calorias")
                {
                    MessageBox.Show("Durante este dia ha comido más " + cat + " de las recomendadas (2000 Kcal).\n" +
                    "considere comer de manera más equilibrada y saludable.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Durante este dia ha comido más " + cat + " de las recomendadas.\n" +
                    "Este valor se calcula en funcion de las calorias ingeridas, \n" +
                    "considere comer productos con más calorias y menos " + cat + " durante el resto del dia.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
            if (numberToCheck < bottom)
            {
                if (cat == "calorias")
                {
                    MessageBox.Show("Durante este dia ha comido menos " + cat + " de las recomendadas (0 Kcal).\n" +
                    "considere comer de manera más equilibrada y saludable.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                MessageBox.Show("Durante este dia ha comido menos " + cat + " de las recomendadas.\n" +
                    "Este valor se calcula en funcion de las calorias ingeridas, \n" +
                    "considere comer productos con más calorias y menos " + cat + ".", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return (numberToCheck >= bottom && numberToCheck <= top);
        }

        #endregion


        //Coge los valores de la resta diaria
        public Actividades restadiari()
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            Modelo.Actividades At = new Modelo.Actividades();
            try
            {
                _conn.Open();
                string sql = string.Format("SELECT saciedad, cansancio, higiene, forma_fisica, salud_mental, conocimientos " +
                    "from actividades where Categorias_idCategorias = 8");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    At.saciedad = (int)rdr["saciedad"];
                    At.cansancio = (int)rdr["cansancio"];
                    At.higiene = (int)rdr["higiene"];
                    At.fisico = (int)rdr["forma_fisica"];
                    At.mental = (int)rdr["salud_mental"];
                    At.conocimiento = (int)rdr["conocimientos"];
                }

                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return At;
        }

        // insertamos la fecha de salida del usuario, nos pasa la fecha y el id del usuario
        public int registrosalida(int id, DateTime data_salida)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int respuesta = 0;
            try
            {
                // consulta SQL
                _conn.Open();
                string sql = "UPDATE Tamagochi SET datasalida= @salida, dataHastaActualizacion = @datahasta where Usuario_idUsuario= @id";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@salida", data_salida);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@datahasta", Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion);

                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        public int ActualizarHoraActualizacion(int id)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int respuesta = 0;
            try
            {
                // consulta SQL
                _conn.Open();
                string sql = "UPDATE Tamagochi SET dataHastaActualizacion = @datahasta where Usuario_idUsuario= @id";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@datahasta", Usuario.Usuario_Actual.Tamagochis.TiempoHastaActualizacion);

                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        //recoger la fecha salida del usuario nos pasa el id usuario
        public DateTime datasalida(int id)
        {

            DateTime data_salida = DateTime.Now;
            string data;
            MySqlConnection _conn = new MySqlConnection(connStr);
            try
            {
                // consulta SQL de la funcion
                _conn.Open();
                string sql = string.Format("SELECT datasalida from tamagochi where Usuario_idUsuario =" + id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    // primero pasamos el resultado a String y luego a datetime
                    data = rdr["datasalida"].ToString();
                    data_salida = Convert.ToDateTime(data);
                }
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            // devolvemos la fecha recogida en datetime
            return data_salida;
        }

        //diferencia de horar entre la salida y la entrada
        public double diferenciaHoras(int id)
        {
            DateTime data_inicio = DateTime.Now;
            DateTime data_salida = datasalida(id);
            var horas = (data_inicio - data_salida).TotalMinutes;
            return horas;
        }

        public double diferenciaDias()
        {
            DateTime data_inicio = DateTime.Now;
            DateTime data_salida = Usuario.Usuario_Actual.Tamagochis.DataRegistro;
            var dias = (data_inicio - data_salida).TotalDays;
            return dias;
        }
    }
}
    


