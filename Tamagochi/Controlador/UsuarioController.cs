﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamagochi.Modelo;
using MySql.Data;
using MySql.Data.MySqlClient;

//bryan

namespace Tamagochi.Controlador
{
    public class UsuarioController
    {
        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";

        //           cmd.CommandType = System.Data.CommandType.StoredProcedure();

        /* Select de la lista donde nombre y contrasenya(codificada) coinciden; 
         * Si lee alguno rellena el usuario "autentificado" con los datos de la base de datos
                                    */
        public Usuario Autentificar(string nombre, string password)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            Usuario autentificado = null;
            string tabla = "usuario";
            try
            {
                string passcd = Codifica.ConverteixPassword(password);

                _conn.Open();
                string sql = string.Format("SELECT * FROM {0} where nombre=@nombre and contrasenya=@contrasenya", tabla);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@contrasenya", passcd);

                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    autentificado = new Usuario();

                    autentificado.Id = (int)rdr["idUsuario"];
                    autentificado.Nombre = rdr["nombre"].ToString();
                    autentificado.Contrasenya = rdr["contrasenya"].ToString();
                    autentificado.Edad = (int)rdr["edad"];
                    autentificado.Peso = (int)rdr["peso"];
                    autentificado.email = rdr["email"].ToString();

                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return autentificado;
        }

        // Generamos lista de todo de todos los Usuario
        public List<Usuario> GetAll()
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            List<Usuario> lista = new List<Usuario>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  Usuario");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Usuario u = new Usuario();
                    u.Id = (int)rdr["idUsuario"];
                    u.Nombre = rdr["Nombre"].ToString();
                    u.Edad = (int)rdr["Edad"];
                    u.Peso = (int)rdr["Peso"];
                    u.Contrasenya = rdr["Contrasenya"].ToString();
                    u.email = rdr["Email"].ToString();


                    lista.Add(u);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return lista;
        }

        // Devuelve el usuario completo con nombre=nombre
        public Usuario GetUserByNombre(string nombre)
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            Usuario u = new Usuario();

            try
            {
                _conn.Open();
                string sql = string.Format(string.Format("SELECT * FROM usuario where Nombre=\"{0}\"", nombre));
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    u.Id = (int)rdr["idUsuario"];
                    u.Nombre = rdr["Nombre"].ToString();
                    u.Edad = (int)rdr["Edad"];
                    u.Peso = (int)rdr["Peso"];
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return u;
        }

        // Comprobamos si nombre esta libre en Base de Datos (devuelve true si está libre)
        public bool NombreLibre(string nombreBuscar)
        {

            foreach (Usuario b in this.GetNombres())
            {

                if (b.Nombre.Equals(nombreBuscar))
                {
                    return false;
                }

            }
            return true;
        }

        //Generamos lista de solo los nombres de Usuario
        public List<Usuario> GetNombres()
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            List<Usuario> listaNombres = new List<Usuario>();

            try
            {

                _conn.Open();
                string sql = string.Format("SELECT Nombre FROM  Usuario");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Usuario u = new Usuario();
                    u.Nombre = rdr["Nombre"].ToString();
                    listaNombres.Add(u);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return listaNombres;
        }

        //Guardar nuevo registro de usuario en la base de datos (con contraseña codificada)
        public int Save(Usuario uno)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            int respuesta = 0;

            try
            {

                _conn.Open();

                string sql = "INSERT INTO Usuario (nombre, edad, peso, contrasenya, email) VALUES (@nombre, @edad, @peso, @contrasenya, @email)";

                string passwordVisible = uno.Contrasenya;
                string passwordCodificada = Codifica.ConverteixPassword(passwordVisible);

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@nombre", uno.Nombre);
                cmd.Parameters.AddWithValue("@edad", uno.Edad);
                cmd.Parameters.AddWithValue("@peso", uno.Peso);
                cmd.Parameters.AddWithValue("@contrasenya", passwordCodificada);
                cmd.Parameters.AddWithValue("@email", uno.email);

                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        //Borramos usuario
        public void borrar(Usuario tama)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int id = tama.Id;  // obtenemos el id del usuario actual

            try
            {
            // consulta SQL de borra el usuario con el ID indicado
                _conn.Open();
                string sql = string.Format("DELETE FROM usuario where idUsuario={0}", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
        }

        //deveria estar en otro controller (ActividadesControler?)
        public void rellenarCombobox( )
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            
           
            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  categorias");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Usuario u = new Usuario();
                    u.Id = (int)rdr["idCategorias"];
                    u.Nombre = rdr["Nombre"].ToString();
                 
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return;

        }


        public int Save2(Usuario tama ,  string password)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            int respuesta = 0;
            int id = tama.Id;

            try
            {
                string passcd = Codifica.ConverteixPassword(password);

                _conn.Open();


                string sql = string.Format("update usuario set nombre=@nombre, Contrasenya=@contra where idusuario ={0} ", id);

                 tama.Contrasenya = passcd;
               

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@nombre", tama.Nombre);
                cmd.Parameters.AddWithValue("@contra", tama.Contrasenya);



                cmd.Prepare();
                respuesta = cmd.ExecuteNonQuery();


            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return respuesta;
        }

        

    }
    
 }

