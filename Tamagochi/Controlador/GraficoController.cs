﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamagochi.Modelo;
using Tamagochi.Vista;
using MySql.Data;
using MySql.Data.MySqlClient;
using Tamagochi.Controlador;
using System.Windows.Forms;

namespace Tamagochi.Controlador
{
    public class GraficoController
    {
        TamagochiController tc = new TamagochiController();

        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";


        // Genero todos los datos de tamagochi ordenados por el data
        public List<Grafico> GetTodoGrafico(int id)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            List<Grafico> informacion = new List<Grafico>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT date(Data) fecha, Tamagochi_idTamagochi, Actividades_idActividades, sum(a.saciedad) saciedad, sum(a.cansancio) cansancio, sum(a.higiene) higiene, sum(a.forma_fisica) forma_fisica, sum(a.salud_mental) salud_mental, sum(a.conocimientos) conocimientos  FROM mydb.tamagochi_has_actividades ta left join actividades a on  a.idActividades=ta.Actividades_idActividades where Tamagochi_idTamagochi = {0} and Data<= now() group by date(Data)", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                        Grafico nuevo = new Grafico();
                        nuevo.IdActividad = (int)rdr["Actividades_idActividades"];
                        nuevo.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                        nuevo.Hambre = Convert.ToInt32(rdr["saciedad"]);
                        nuevo.Cansancio = Convert.ToInt32(rdr["cansancio"]);
                        nuevo.Higiene = Convert.ToInt32(rdr["higiene"]);
                        nuevo.FormaFisica = Convert.ToInt32(rdr["forma_fisica"]);
                        nuevo.SaludMental = Convert.ToInt32(rdr["salud_mental"]);
                        nuevo.Conocimientos = Convert.ToInt32(rdr["conocimientos"]);
                        //nuevo.MediaGeneral = Convert.ToInt32(rdr["media_general"]);
                        string algo = rdr["fecha"].ToString();
                        nuevo.Fecha = Convert.ToDateTime(algo);

                        informacion.Add(nuevo);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                string msg = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return informacion;
        }




        public Grafico grafico(int id, string data)
        {
            //data = "2018-03-08";
            //id = 19;
            
            Grafico a = GetSumEstadosActividades(id, data);

            DateTime data_grafic = Convert.ToDateTime(a.Fecha);
            DateTime data_alta = Usuario.Usuario_Actual.Tamagochis.DataRegistro;

            //data_alta = data_alta.ToString("yyyy-MM-dd");

            TimeSpan ts = new TimeSpan();

            ts = data_grafic.Subtract(data_alta);

            double dias_pasados = data_grafic.Subtract(data_alta).TotalHours;

            dias_pasados = dias_pasados / 5;
            int horas = (int)dias_pasados;
            
            a.FormaFisica += 250 + (tc.restadiari().fisico * horas);
            a.Higiene += 250 + (tc.restadiari().higiene * horas);
            a.Cansancio += 250 + (tc.restadiari().cansancio * horas);
            a.Hambre += 250 + (tc.restadiari().saciedad * horas);
            a.SaludMental += 250 + (tc.restadiari().mental * horas);
            a.Conocimientos += 250 + (tc.restadiari().conocimiento * horas);

            return a;
        }

        public List<Grafico> listgrafico(int id, string data)
        {
            //data = "2018-03-08";
            //id = 19;

            List<Grafico> ResultadoGrafico = new List<Grafico>();



            List<Grafico> lista_estados = GetEstadosTamagochi(id, data);

            foreach (Grafico a in lista_estados)
            {
                DateTime data_grafic = Convert.ToDateTime(a.Fecha);
                DateTime data_alta = Usuario.Usuario_Actual.Tamagochis.DataRegistro;
                double dias_pasados = (data_grafic - data_alta).TotalDays;
                int dias = (int)dias_pasados;

                a.FormaFisica += 250 +  (tc.restadiari().fisico * dias * 5);
                a.Higiene += 250 + (tc.restadiari().higiene * dias * 5);
            }
        
            return lista_estados;
        }

        // devuelve una actividad('Grafico') con la suma de todas las variaciones de estado de todas
        // las actividades hechas des del inicio hasta la fecha 'data'
        public Grafico GetSumEstadosActividades(int id, string data)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            Grafico nuevo = new Grafico();
            try
            {
                _conn.Open();
                string sql = string.Format("select date(data) fecha, Actividades_idActividades actividad, Tamagochi_idTamagochi tamagochi, " +
                    "sum(a.saciedad) hambre, sum(a.cansancio) cansancio, sum(a.higiene) higiene, sum(a.forma_fisica) forma_fisica, sum(a.salud_mental) salud_mental, sum(a.conocimientos) conocimientos " +
                    "FROM mydb.tamagochi_has_actividades ta " +
                    "left join actividades a on  a.idActividades = ta.Actividades_idActividades where Tamagochi_idTamagochi = {0} " +
                    "and date(data) <= '{1}' group by date(data)", id, data);

                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                string fecha="";
                while (rdr.Read())
                {
                    nuevo.IdActividad = (int)rdr["actividad"];
                    nuevo.IdTamagochi = (int)rdr["tamagochi"];
                    nuevo.Hambre += Convert.ToInt32(rdr["hambre"]);
                    nuevo.Cansancio += Convert.ToInt32(rdr["cansancio"]);
                    nuevo.Higiene += Convert.ToInt32(rdr["higiene"]);
                    nuevo.FormaFisica += Convert.ToInt32(rdr["forma_fisica"]);
                    nuevo.SaludMental += Convert.ToInt32(rdr["salud_mental"]);
                    nuevo.Conocimientos += Convert.ToInt32(rdr["conocimientos"]);

                    fecha = rdr["fecha"].ToString();
                    nuevo.Fecha = Convert.ToDateTime(fecha);
                }
                if (fecha == "")
                {
                    nuevo.Hambre += 0;
                    nuevo.Cansancio += 0;
                    nuevo.Higiene += 0;
                    nuevo.FormaFisica += 0;
                    nuevo.SaludMental += 0;
                    nuevo.Conocimientos += 0;
                    nuevo.Fecha = Convert.ToDateTime(data);
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                string msg = ex.Message;
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return nuevo;
        }

        // devuelve estados de tamagochi sumados
        public List<Grafico> GetEstadosTamagochi(int id, string data)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            List<Grafico> lista = new List<Grafico>();
            
            try
            {
                _conn.Open();
                string sql = string.Format("select date(data) fecha, Actividades_idActividades actividad, Tamagochi_idTamagochi tamagochi, sum(a.saciedad) hambre, sum(a.cansancio) cansancio, sum(a.higiene) higiene, sum(a.forma_fisica) forma_fisica, sum(a.salud_mental) salud_mental, sum(a.conocimientos) conocimientos FROM mydb.tamagochi_has_actividades ta left join actividades a on  a.idActividades = ta.Actividades_idActividades where Tamagochi_idTamagochi = {0} and date(data) <= '{1}' group by date(data)", id, data);

                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Grafico nuevo = new Grafico();
                    nuevo.IdActividad = (int)rdr["actividad"];
                    nuevo.IdTamagochi = (int)rdr["tamagochi"];
                    nuevo.Hambre = Convert.ToInt32(rdr["hambre"]);
                    nuevo.Cansancio = Convert.ToInt32(rdr["cansancio"]);
                    nuevo.Higiene = Convert.ToInt32(rdr["higiene"]);
                    nuevo.FormaFisica = Convert.ToInt32(rdr["forma_fisica"]);
                    nuevo.SaludMental = Convert.ToInt32(rdr["salud_mental"]);
                    nuevo.Conocimientos = Convert.ToInt32(rdr["conocimientos"]);
                    string algo = rdr["fecha"].ToString();
                    nuevo.Fecha = Convert.ToDateTime(algo);
                    lista.Add(nuevo);
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                string msg = ex.Message;
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return lista;
        }

    }
}
