﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;

namespace Tamagochi.Controlador
{
    class ListaActividadesController
    {
        DataTable dt;
        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";

        //Creamos una lista con todos los atributos de Lista de Actividades
        public List<ListaActividades> GetAll()
        {
            List<ListaActividades> lista = new List<ListaActividades>();
            MySqlConnection _conn = new MySqlConnection(connStr);
            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  Tamagochi_has_Actividades");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaActividades list = new ListaActividades();
                    list.Fecha = (DateTime)rdr["Data"];
                    list.IdActividades = (int)rdr["Actividades_idActividades"];
                    list.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                    lista.Add(list);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return lista;
        }


        //Creamos una lista con todos los atributos de Tamagochi_has_actividades para el Historial (LEFTJoin)
        public List<ListaActividades> GetAllParaHistorial()
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
           
            List<ListaActividades> listagetall = new List<ListaActividades>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  tamagochi_has_actividades Thas" +
                    " left join actividades act on Thas.Actividades_idActividades=act.idActividades" +
                    " where Tamagochi_idTamagochi=" + Usuario.Usuario_Actual.Tamagochis.Id + 
                    " Order by Data desc");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaActividades list = new ListaActividades();
                    list.Fecha = (DateTime)rdr["Data"];
                    list.IdActividades = (int)rdr["Actividades_idActividades"];
                    list.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                    list.NombreActividad = rdr["nombre"].ToString();
                    list.Duracion = (int)rdr["Duracion"];
                    listagetall.Add(list);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return listagetall;
        }

        //Creamos una lista con todos los atributos de Tamagochi_has_actividades en el dia seleccionado para el Historial (LEFTJoin)
        public List<ListaActividades> GetDiaParaHistorial(string data)
        {

            MySqlConnection _conn = new MySqlConnection(connStr);          
            List<ListaActividades> listagetday = new List<ListaActividades>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  tamagochi_has_actividades Thas" +
                    " left join actividades act on Thas.Actividades_idActividades=act.idActividades" +
                    " where Tamagochi_idTamagochi=" + Usuario.Usuario_Actual.Tamagochis.Id +
                    " and date(Data) = '" + data + "' Order by Data desc");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaActividades list = new ListaActividades();
                    list.Fecha = (DateTime)rdr["Data"];
                    list.IdActividades = (int)rdr["Actividades_idActividades"];
                    list.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                    list.NombreActividad = rdr["nombre"].ToString();
                    list.Duracion = (int)rdr["Duracion"];
                    listagetday.Add(list);
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return listagetday;
        }

        //Método guardar lista de Actividades
        public int Guardar(ListaActividades listact)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            int res = 0;

            try
            {

                _conn.Open();

                string sql = "INSERT INTO Tamagochi_has_Actividades (IdActividades, IdTamagochi, Fecha) VALUES (@idactividades, @idtamagochi, @data)";

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@idactividades", listact.IdActividades);
                cmd.Parameters.AddWithValue("@idtamagochi", listact.IdTamagochi);
                cmd.Parameters.AddWithValue("@data", listact.Fecha);

                cmd.Prepare();
                res = cmd.ExecuteNonQuery();


            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }

            return res;
        }
        
        //Método para añadir una actividad a la lista de actividades
        public int Añadir(int idactividades, DateTime fecha, int idtamagochi, int Duracion)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int rs = 0;
            try
            {
                _conn.Open();

                string sql = "INSERT INTO Tamagochi_has_Actividades (Tamagochi_idTamagochi, Actividades_idActividades, Data, Duracion) VALUES (@idTamagochi, @idActividades, @idData, @Duracion)";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
       
                cmd.Parameters.AddWithValue("@idActividades", idactividades);
                cmd.Parameters.AddWithValue("@idTamagochi", idtamagochi);
                cmd.Parameters.AddWithValue("@idData", fecha);
                cmd.Parameters.AddWithValue("@Duracion", Duracion);

                cmd.Prepare();
                rs = cmd.ExecuteNonQuery();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return rs;
        }

        //Generamos solo id's de actividades
        public List<ListaActividades> GetIdActividades()
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            List<ListaActividades> listaIdActividades = new List<ListaActividades>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT IdActividades FROM  ListaActividades");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaActividades l = new ListaActividades();
                    l.IdActividades = (int)rdr["IdActividades"];


                    listaIdActividades.Add(l);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return listaIdActividades;
        }

        //Método que modifica los datos de una actividad en la base de datos
        public int ModificarDB(int idactividades, DateTime fecha, int idtamagochi)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int rs = 0;
            try
            {
                _conn.Open();

                string sql = "UPDATE InformacionCompañias SET IdActividad= '" + idactividades + "', Fecha= " + fecha + ", IdTamagochi = '" + idtamagochi + " WHERE IdActividad= '" + idactividades + "'";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                rs = cmd.ExecuteNonQuery();

                if (rs != 1)
                    return -1;

                else
                    return 0;
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return rs;

        }

        //Método para comprobar si el idactividad está ocupado en la DataBase
        public bool Consultar(string nom)
        {
            foreach (ListaActividades l in this.GetIdActividades())
            {

                if (l.IdActividades.Equals(nom))
                {
                    return false;
                }

            }
            return true;
        }

        /* Borrar
        //Método que permite borrar la actividad actual de la lista
        public int BorrarActividad(int idactividad)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int rs = 0;
            try
            {
                //Consulta la SQL y borra la actividad con el ID indicado
                _conn.Open();
                string sql = string.Format("DELETE FROM ListaActividades WHERE  IdActividad= '" + idactividad + "'");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                rs = cmd.ExecuteNonQuery();

                if (rs != 1)
                    return -1;

                else
                    return 0;
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return rs;
        }

        //Método que permite borrar todas las actividades de la lista
        public void Borrar(ListaActividades llista)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int id = llista.IdActividades;  //Obtenemos el id de la actividad actual

            try
            {
                //Consulta la SQL y borra la actividad con el ID indicado
                _conn.Open();
                string sql = string.Format("DELETE FROM ListaActividades");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
        }
        */


        public void Historial(DataGridView dgv)
        {
            MySqlConnection _conn = new MySqlConnection();

            try
            {
                _conn.Open();
                MySqlDataAdapter adp = new MySqlDataAdapter("SELECT * FROM ListaActividades", _conn);
                adp.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception exep)
            {
                MessageBox.Show("No se pudo completar el DataGridView:" + exep.ToString());
            }
        }

        //Método que cierra la base de datos       
        public void CloseDB(/*MySqlConnection _conn*/) // passar-li una conexió // potser no cal
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            if (_conn != null)
            {
                _conn.Close();
                _conn = null;
            }
        }
    }
}
