﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamagochi.Modelo;

namespace Tamagochi.Controlador
{
    class ListaComidasController
    {
        DataTable dt;
        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";

        public int Añadir(int idcomidas, DateTime fecha,int racion, int idtamagochi)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int rs = 0;
            try
            {
                _conn.Open();

                string sql = "INSERT INTO tamagochi_has_comidas (Tamagochi_idTamagochi, Comidas_idComidas,Cantidad_comidas, Date) VALUES (@idTamagochi, @idComidas,@cantidad ," +
                    "@idDate)";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@idComidas",idcomidas);
                cmd.Parameters.AddWithValue("@idTamagochi", idtamagochi);
                cmd.Parameters.AddWithValue("@Cantidad", racion);
                cmd.Parameters.AddWithValue("@idDate", fecha);

                cmd.Prepare();
                rs = cmd.ExecuteNonQuery();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return rs;
        }

        //Creamos una lista con todos los atributos de Tamagochi_has_actividades para el Historial (LEFTJoin)
        public List<ListaComidas> GetAllHistorialComidas()
        {
            MySqlConnection _conn = new MySqlConnection(connStr);

            List<ListaComidas> listagetcom = new List<ListaComidas>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM tamagochi_has_comidas Tcom" +
                    " LEFT JOIN comidas com ON Tcom.Comidas_idComidas=com.idcomidas" +
                    " WHERE Tamagochi_idTamagochi=" + Usuario.Usuario_Actual.Tamagochis.Id +
                    " Order by Date desc");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaComidas list = new ListaComidas();
                    list.Fecha = (DateTime)rdr["Date"];
                    list.IdComidas = (int)rdr["Comidas_idComidas"];
                    list.Racion = (int)rdr["Cantidad_Comidas"];
                    list.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                    list.NombreComidas = rdr["nombre"].ToString();
                    listagetcom.Add(list);
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return listagetcom;
        }
        public void CloseDB()
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            if (_conn != null)
            {
                _conn.Close();
                _conn = null;
            }
        }

        //Creamos una lista con todos los atributos de Tamagochi_has_actividades en el dia seleccionado para el Historial (LEFTJoin)
        public List<ListaComidas> GetDiaParaHistorialComidas(string data)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            List<ListaComidas> listagetday = new List<ListaComidas>();

            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  tamagochi_has_comidas Thas" +
                    " left join comidas com on Thas.Comidas_idComidas=com.idcomidas" +
                    " where Tamagochi_idTamagochi=" + Usuario.Usuario_Actual.Tamagochis.Id +
                    " and date(Date) = '" + data + "' Order by Date desc");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListaComidas list = new ListaComidas();
                    list.Fecha = (DateTime)rdr["Date"];
                    list.IdComidas = (int)rdr["Comidas_idComidas"];
                    list.Racion = (int)rdr["Cantidad_Comidas"];
                    list.IdTamagochi = (int)rdr["Tamagochi_idTamagochi"];
                    list.NombreComidas = rdr["nombre"].ToString();
                    listagetday.Add(list);
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return listagetday;
        }


        //Método para añadir una actividad a la lista de actividades
        public int AñadirComida(int idcomidas , DateTime fecha , int racion, int idtamagochi)
        {
            MySqlConnection _conn = new MySqlConnection(connStr);
            int rs = 0;
            try
            {
                _conn.Open();

                string sql = "INSERT INTO Tamagochi_has_Comidas (Tamagochi_idTamagochi, Comidas_idComidas,Cantidad_Comidas, Date) VALUES (@idTamagochi, @idcomidas,@cantidad, @idDate)";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@idcomidas", idcomidas);
                cmd.Parameters.AddWithValue("@idTamagochi", idtamagochi);
                cmd.Parameters.AddWithValue("@cantidad",racion );
                cmd.Parameters.AddWithValue("@idDate", fecha);

                cmd.Prepare();
                rs = cmd.ExecuteNonQuery();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    CloseDB();
                }
            }
            return rs;
        }
    }
}
