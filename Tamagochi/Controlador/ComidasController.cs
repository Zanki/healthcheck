﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tamagochi.Modelo;

namespace Tamagochi.Controlador
{
    class ComidasController
    {
        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";

        public void RellenarCatComida(ComboBox cb)
        {
            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                MySqlCommand cm = new MySqlCommand("Select nombre from categoriascomidas", cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                while (rd.Read())
                {
                    cb.Items.Add(rd["nombre"].ToString());
                }
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
        }

        public void RellenarByCategoriaComidas(ComboBox cb, int id)
        {
            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                MySqlCommand cm = new MySqlCommand(string.Format("Select * from comidas where categoriasComidas={0}", id), cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                while (rd.Read())
                {
                    cb.Items.Add(rd["nombre"].ToString());
                }
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

        }

        public Comidas GetValuesFromComida(string nombrecom)
        {
            //string list = "calorias, grasas, azucares";
            Comidas comida = new Comidas();

            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                string query = string.Format("Select * from comidas where nombre =\"{0}\"", nombrecom);

                MySqlCommand cm = new MySqlCommand(query, cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                if (rd.Read())
                {
                    comida.Id = (int)rd["idcomidas"];
                    comida.Nombre = rd["nombre"].ToString();
                    comida.Cal = (float)rd["calorias"];
                    comida.Grasas = (float)rd["grasas"];
                    comida.Azucares = (float)rd["azucares"];
                    comida.IdCategoriasComidas = (int)rd["categoriasComidas"];
                }
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
            return comida;
        }

        public void rellenarComboboxComida(ComboBox cb)
        {
            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                MySqlCommand cm = new MySqlCommand("Select Nombre from categoriascomidas", cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                while (rd.Read())
                {
                    cb.Items.Add(rd["Nombre"].ToString());
                }
                cb.SelectedItem = 0;
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

        }


        public Comidas GetCalPerDay()
        {
            DateTime dia = DateTime.Now;
            string diahoy = dia.ToString("yyyy-MM-dd");

            MySqlConnection _conn = new MySqlConnection(connStr);
            Comidas com = new Comidas();

            try
            {
                _conn.Open();
                
                /*
                string sql = string.Format("select date(date) fecha, " +
                    "sum(com.calorias) calorias, sum(com.grasas) grasas, sum(com.azucares) azucares " +
                    "FROM tamagochi_has_comidas tc " +
                    "left join comidas com on com.idcomidas = tc.Comidas_idComidas " +
                    "where Tamagochi_idTamagochi = {0} and date(date) = '{1}' " +
                    "group by date(date)", Usuario.Usuario_Actual.Tamagochis.Id, diahoy);
                */

                int cantidad = 0;
                string sql = string.Format("select date(date) fecha, sum(Cantidad_Comidas) as cantidad, " +
                    "com.calorias calorias, com.grasas grasas, com.azucares azucares " +
                    "FROM tamagochi_has_comidas tc " +
                    "left join comidas com on com.idcomidas = tc.Comidas_idComidas " +
                    "where Tamagochi_idTamagochi = {0} and date(date) = '{1}' " +
                    "group by Comidas_idComidas", Usuario.Usuario_Actual.Tamagochis.Id, diahoy);

                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    cantidad = Convert.ToInt32(rdr["cantidad"]);
                    com.Cal += (float)(rdr["calorias"])*cantidad;
                    com.Grasas += (float)(rdr["grasas"])*cantidad;
                    com.Azucares += (float)(rdr["azucares"])*cantidad;
                }
                rdr.Close();
            }
            catch (FormatException ex)
            {
                string msg = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }

            return com;
        }

    }
}
