﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamagochi.Modelo;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Tamagochi.Vista;
using System.Threading;

namespace Tamagochi.Controlador
{
   public class ActividadesController
    {

        private const string connStr = "server=192.168.1.57;user=root;database=mydb;port=3306;password=root";
         List<Actividades> lista = new List<Actividades>();

        public List<Actividades> GetAll()
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            try
            {

                _conn.Open();
                string sql = string.Format("SELECT * FROM  Usuario");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Actividades a = new Actividades();
                    a.id = (int)rdr["idActividad"];
                    a.nombre = rdr["Nombre"].ToString();
                    a.saciedad = (int)rdr["Edad"];
                    a.cansancio = (int)rdr["Peso"];
                    a.higiene = (int)rdr["Contrasenya"];
                    a.fisico = (int)rdr["Peso"];
                    a.mental = (int)rdr["Peso"];
                    a.conocimiento = (int)rdr["Peso"];
                    a.categoria = (int)rdr["Peso"];

                    lista.Add(a);
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return lista;
        }

        // Get idcategoria a partir del nombre introducido
        public int GetIdByName(string nameAct)
        {

            MySqlConnection _conn = new MySqlConnection(connStr);
            int idAct = 0;
            try
            {
                    

                _conn.Open();
                string sql = string.Format("SELECT idCategorias FROM  categorias where Nombre=\"{0}\"", nameAct);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    idAct = (int)rdr["idCategorias"];
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return idAct;
        }

        // Get que devuelve la acitividad que el usuario pasa como parámetro
        public Actividades GetActividadByName(string nameAct)
        {
            Actividades act = new Actividades();

            MySqlConnection _conn = new MySqlConnection(connStr);
 
            try
            {
                _conn.Open();
                string sql = string.Format("SELECT * FROM  actividades where Nombre=\"{0}\"", nameAct);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    act.id = (int)rdr["idActividades"];
                    act.nombre = rdr["nombre"].ToString();
                    act.saciedad = (int)rdr["saciedad"];
                    act.cansancio = (int)rdr["cansancio"];
                    act.higiene = (int)rdr["higiene"];
                    act.fisico = (int)rdr["forma_fisica"];
                    act.mental = (int)rdr["salud_mental"];
                    act.conocimiento = (int)rdr["conocimientos"];
                    act.categoria = (int)rdr["Categorias_idCategorias"];
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return act;
        }

        //Devuelve todos los nombres que aparecen en la tabla categorias
        public void rellenarCombobox(ComboBox cb)
        {
            MySqlConnection cnn= new MySqlConnection (connStr);
            try {
                cnn.Open();

                MySqlCommand cm = new MySqlCommand("Select Nombre from categorias where Nombre <> \"Resta diaria\"", cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                while (rd.Read())
                {
                    cb.Items.Add(rd["Nombre"].ToString());
                }
                cb.SelectedItem = 0;
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

        }

        //Se pasa por parámetro el idcategoria y nos devuelve la actividad con esa misma id
        public void RellenarByCategoriaId(ComboBox cb, int id)
        {
            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                MySqlCommand cm = new MySqlCommand(string.Format("Select * from actividades where Categorias_idCategorias={0}", id), cnn);
                MySqlDataReader rd = cm.ExecuteReader();

                while (rd.Read())
                {
                    cb.Items.Add(rd["nombre"].ToString());
                }
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }

        }

        //Método que vacía la combobox
        public void VaciarComboBox(ComboBox cb)
        {
            cb.SelectedText = "";
            cb.SelectedValue = 0;

            cb.Items.Clear();
        }

        //A partir del nombre de la actividad que se ha introducido se devuelven los valores de esta
        public int[] GetValuesFromActivity(string nombreact)
        {
            string list = "saciedad, cansancio, higiene, forma_fisica, salud_mental, conocimientos";

            int[] val = new int[9] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            MySqlConnection cnn = new MySqlConnection(connStr);
            try
            {
                cnn.Open();

                string query = string.Format("Select {0} from actividades where nombre =\"{1}\"", list,  nombreact);

                MySqlCommand cm = new MySqlCommand(query, cnn);
                MySqlDataReader rd = cm.ExecuteReader();


                while (rd.Read())
                {
                    val[0] = (int)rd["saciedad"];
                    val[1] = (int)rd["cansancio"];
                    val[2] = (int)rd["higiene"];
                    val[3] = (int)rd["forma_fisica"];
                    val[4] = (int)rd["salud_mental"];
                    val[5] = (int)rd["conocimientos"];        
                }
                rd.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                if (cnn != null && cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
            return val;
        }

        //Método que modifica los gits según la actividad que se realiza
        public async Task CambioImagenActividad(TamagochiCentral ta, int actvId)
        {
            switch (actvId)
            {
                case 1:
                    ta.pictureBox1.Image = Properties.Resources.Eat;
                    break;
                case 2:
                    ta.pictureBox1.Image = Properties.Resources.Sleep;                 
                    break;
                case 3:
                    ta.pictureBox1.Image = Properties.Resources.wash;               
                    break;
                case 4:
                    ta.pictureBox1.Image = Properties.Resources.ocio;
                    break;
                case 5:
                    ta.pictureBox1.Image = Properties.Resources.work;
                    break;
                case 6:
                    ta.pictureBox1.Image = Properties.Resources.drive;
                    break;
                case 7:
                    ta.pictureBox1.Image = Properties.Resources.jump;
                    break; 
                default:
                    ta.pictureBox1.Image = Properties.Resources.presente;
                    break;
            }
            ta.pictureBox1.Update();
            await Task.Delay(8000);
            if (Usuario.Usuario_Actual.Tamagochis.Hambre >= 400){
                ta.pictureBox1.Image = Properties.Resources.relleno;
            }
            else
            {
                ta.pictureBox1.Image = Properties.Resources.presente;
            }
            ta.pictureBox1.Update();
        }
        
    }
}
